useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 2,084.499/3,000.00 = 0.694833
testsetLikelihood(sum(2,084.499), countOfExamples(3,000.00), mean(0.694833)).

// The weighted count of positive examples = 684.000 and the weighted count of negative examples = 2,316.000
weightedSumPos(684.000).
weightedSumNeg(2,316.000).

//  AUC ROC   = 0.500000
//  AUC PR    = 0.228000
//  CLL       = -0.563378
aucROC(gender, 0.500000).
aucPR( gender, 0.228000).

//   Precision = 0.228000 at threshold = 0.000
//   Recall    = 1.000000
//   F1        = 0.371336
precision(gender, 0.228000, usingThreshold(0.0)).
recall(   gender, 1.000000, usingThreshold(0.0)).
F1(       gender, 0.371336, usingThreshold(0.0)).

