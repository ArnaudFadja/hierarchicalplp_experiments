useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 2,103.123/3,000.00 = 0.701041
testsetLikelihood(sum(2,103.123), countOfExamples(3,000.00), mean(0.701041)).

// The weighted count of positive examples = 658.000 and the weighted count of negative examples = 2,342.000
weightedSumPos(658.000).
weightedSumNeg(2,342.000).

//  AUC ROC   = 0.500000
//  AUC PR    = 0.219333
//  CLL       = -0.547778
aucROC(gender, 0.500000).
aucPR( gender, 0.219333).

//   Precision = 0.219333 at threshold = 0.000
//   Recall    = 1.000000
//   F1        = 0.359759
precision(gender, 0.219333, usingThreshold(0.0)).
recall(   gender, 1.000000, usingThreshold(0.0)).
F1(       gender, 0.359759, usingThreshold(0.0)).

