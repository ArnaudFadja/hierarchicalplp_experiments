useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 2,128.193/3,000.00 = 0.709398
testsetLikelihood(sum(2,128.193), countOfExamples(3,000.00), mean(0.709398)).

// The weighted count of positive examples = 623.000 and the weighted count of negative examples = 2,377.000
weightedSumPos(623.000).
weightedSumNeg(2,377.000).

//  AUC ROC   = 0.500000
//  AUC PR    = 0.207667
//  CLL       = -0.526778
aucROC(gender, 0.500000).
aucPR( gender, 0.207667).

//   Precision = 0.207667 at threshold = 0.000
//   Recall    = 1.000000
//   F1        = 0.343914
precision(gender, 0.207667, usingThreshold(0.0)).
recall(   gender, 1.000000, usingThreshold(0.0)).
F1(       gender, 0.343914, usingThreshold(0.0)).

