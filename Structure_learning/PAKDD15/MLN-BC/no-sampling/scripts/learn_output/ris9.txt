args[0] = -target
args[2] = -trees
args[4] = -negPosRatio
args[6] = -save
args[7] = -l
args[8] = -train
args[10] = -mln
args[11] = -mlnClause
args[12] = -numMLNClause
args[14] = -mlnClauseLen
train9

% Calling SETUP.
% Running on host: r240n01

% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.

Resetting the LazyGroundNthArgumentClauseIndex.

% Calling ILPouterLoop from createRegressionOuterLooper.

% getInputArgWithDefaultValue: args=[../data/train9/train9_pos.txt, ../data/train9/train9_neg.txt, ../data/train9/train9_bk.txt, ../data/train9/train9_facts.txt]
%  for N=0: args[N]=../data/train9/train9_pos.txt

% getInputArgWithDefaultValue: args=[../data/train9/train9_pos.txt, ../data/train9/train9_neg.txt, ../data/train9/train9_bk.txt, ../data/train9/train9_facts.txt]
%  for N=1: args[N]=../data/train9/train9_neg.txt

% getInputArgWithDefaultValue: args=[../data/train9/train9_pos.txt, ../data/train9/train9_neg.txt, ../data/train9/train9_bk.txt, ../data/train9/train9_facts.txt]
%  for N=2: args[N]=../data/train9/train9_bk.txt

% getInputArgWithDefaultValue: args=[../data/train9/train9_pos.txt, ../data/train9/train9_neg.txt, ../data/train9/train9_bk.txt, ../data/train9/train9_facts.txt]
%  for N=3: args[N]=../data/train9/train9_facts.txt

% Welcome to the WILL ILP/SRL systems.


% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.
% Reading background theory from dir: null
% Load '../KDD15_bk.txt'.

% Switching to VarIndicator = uppercase.

% Switching to standard-logic notation for variables; previous setting = uppercase

% Switching to VarIndicator = lowercase.

***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****


***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****

% [ LazyGroundClauseIndex ]  Building full index for mode/1 with 1 assertions.
% LoadAllModes() called.  Currently loaded modes: []

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.
% [ LazyGroundClauseIndex ]  Building full index for sameAs/2 with 2 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for exp/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for log/3.
% LoadAllLibraries() called.  Currently loaded libraries: [listsInLogic, differentInLogic, modes_arithmeticInLogic, inlines_comparisonInLogic, modes_listsInLogic, inlines_differentInLogic, modes_differentInLogic, arithmeticInLogic, inlines_listsInLogic, modes_comparisonInLogic, comparisonInLogic, inlines_arithmeticInLogic]

%  Read the facts.
%  Have read 93,101 facts.
% Have read 5,946 examples from '../data/train9' [../data/train9/train9*].
% Have read 21,054 examples from '../data/train9' [../data/train9/train9*].

%  LearnOneClause initialized.

% The outer looper has been created.

% Initializing the ILP inner looper.

% NEW target:                 gender(a, b, c)
%  targetPred:                gender/3
%  targetArgTypes:            signature = [Const, Const, Const], types = [+Fold, +Name, +Gender]
%  targets:                   [gender(a, b, c)]
%  targetPredicates:          [gender/3]
%  targetArgSpecs:            [[a[+Fold], b[+Name], c[+Gender]]]
%  variablesInTargets:        [[a, b, c]]

% Started collecting constants

% Collecting the types of constants.

% Looking at the training examples to see if any types of new constants can be inferred.
% Time to collect constants: 11 seconds
% Time to collect examples: 0 seconds

% Read 5,946 pos examples and 21,054 neg examples.
% Time to init learnOneClause: 12 seconds
% Old dirnull
Setting model dir

% Have 5,946 'raw' positive examples and kept 5,946.
% Have 21,054 'raw' negative examples and kept 21,054.

% processing backup's for gender
%  POS EX = 5,946
%  NEG EX = 21,054

% Memory usage by WILLSetup (just counts # targets?):
%  |backupPosExamples| = 1
%  |backupNegExamples| = 1
%  |predicatesAsFacts| = 0
%  |addedToFactBase|   = 0
../data/train9/models/
File: ../data/train9/advice.txt doesnt exist.Hence no advice loaded
% Learning 20 trees in this iteration for gender

% Learn model for: gender
% Kept 5,946 of the 5,946 positive examples.
% Kept 21,054 of the 21,054 negative examples.
% Dataset size: 27,000
Computing probabilities
prob time:673 milliseconds
No hidden examples for : gender
Time to build dataset: 791 milliseconds
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.
% Have selected pos example #16,772 as the next seed: gender(F5, U27531, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% LearnOneClause Parameters:
%   Targets (1):
%    gender(+Fold, +Name, +Gender)
%  Modes (46):
%    user(+Fold, +Name),
%    tim(+Fold, +Name, -Numb),
%    viewed(+Fold, +Name, -Id),
%    item(+Fold, +Id),
%    ic1(+Fold, +Id, -Idc1),
%    ic2(+Fold, +Id, -Idc2),
%    ic3(+Fold, +Id, -Idc3),
%    c1(+Fold, +Idc1),
%    c2(+Fold, +Idc2),
%    c3(+Fold, +Idc3),
%    addList(+willList, #willNumber),
%    multiplyList(+willList, #willNumber),
%    abs(+willNumber, &willNumber),
%    minus(+willNumber, &willNumber),
%    minus(+willNumber, +willNumber, &willNumber),
%    plus(+willNumber, +willNumber, &willNumber),
%    mult(+willNumber, +willNumber, &willNumber),
%    div(+willNumber, +willNumber, &willNumber),
%    allNumbers(+willList),
%    positiveNumber(+willNumber),
%    negativeNumber(+willNumber),
%    in0toDot001(+willNumber),
%    in0toDot01(+willNumber),
%    in0toDot1(+willNumber),
%    in0to1(+willNumber),
%    in0to10(+willNumber),
%    in0to100(+willNumber),
%    in0to1000(+willNumber),
%    equalWithTolerance(+willNumber, +willNumber, &willNumber),
%    greaterOrEqualDifference(+willNumber, +willNumber, &willNumber),
%    smallerOrEqualDifference(+willNumber, +willNumber, &willNumber),
%    isaEqualTolerance(+willNumber),
%    lessThan(+willNumber, +willNumber),
%    greaterThan(+willNumber, +willNumber),
%    lessThanOrEqual(+willNumber, +willNumber),
%    greaterThanOrEqual(+willNumber, +willNumber),
%    inBetweenOO(+willNumber, +willNumber, +willNumber),
%    inBetweenCO(+willNumber, +willNumber, +willNumber),
%    inBetweenOC(+willNumber, +willNumber, +willNumber),
%    inBetweenCC(+willNumber, +willNumber, +willNumber),
%    memberOfList(+willAnything, +willList),
%    firstInList(+willList, &willAnything),
%    restOfList(+willList, &willList),
%    positionInList(+willAnything, +willList, &willNumber),
%    nthInList(+willNumber, +willList, &willAnything),
%    lengthOfList(+willList, &willNumber)

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #1, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
% [ LazyGroundClauseIndex ]  Building full index for user/2 with 27,000 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for tim/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for tim/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for viewed/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for viewed/3.
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #7,258 as the next seed: gender(F1, U25967, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #2, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #2,592 as the next seed: gender(F5, U11217, Male)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #3, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #19,848 as the next seed: gender(F6, U37521, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #4, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #7,361 as the next seed: gender(F1, U27299, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #5, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #21,089 as the next seed: gender(F7, U23631, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #6, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #20,407 as the next seed: gender(F7, U15030, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #7, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #20,579 as the next seed: gender(F7, U16870, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #8, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #20,865 as the next seed: gender(F7, U20683, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #9, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #16,137 as the next seed: gender(F5, U19604, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #10, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #25,851 as the next seed: gender(F10, U25724, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #11, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #19,194 as the next seed: gender(F6, U28943, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #12, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #2,526 as the next seed: gender(F4, U38733, Male)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #13, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #9,005 as the next seed: gender(F2, U18676, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #14, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #2,535 as the next seed: gender(F4, U38993, Male)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #15, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #17,124 as the next seed: gender(F5, U31991, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #16, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #10,671 as the next seed: gender(F2, U39486, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #17, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #24,281 as the next seed: gender(F8, U35129, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #18, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #23,270 as the next seed: gender(F8, U21660, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #19, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null
% Have selected pos example #17,394 as the next seed: gender(F5, U35329, Female)
% [AdviceProcessor]  Generated 0 clauses at relevance level STRONGLY_IRRELEVANT.

% target           = gender(a, b, c)
%     Score = -Infinity (regressionFit = Infinity, penalties=3.3E-7) for clause:  gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
% Most-general root: gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]  score = -Infinity

% Consider expanding [#1 of outerLoop #20, bodyLen=0] 'gender(_, _, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]' score=-Infinity
%  At # nodes expanded = 1, |OPEN| = 0.  Pruned 0 variant children.  Sending 3 items to OPEN for evaluation and possible insertion.
% Have created 3 valid-on-seeds descendants.
%     Score = -Infinity (regressionFit = Infinity, penalties=1.1300000000000002E-6) for clause:  user(a, b) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  tim(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]
%     Score = -Infinity (regressionFit = Infinity, penalties=1.24E-6) for clause:  viewed(a, b, _) => gender(a, b, _).  [covers 27,000.0/27,000.0 pos, 0.0/0.0 neg]

% The best node found: null

% No acceptable clause was learned on this cycle of the ILP inner loop (LearnOneClause).
% The closest-to-acceptable node found (score = -Infinity):
%  null

% ******************************************

% Have stopped ILP's outer loop because have reached the maximum number of iterations (20).

% ******************************************
adding regression values
