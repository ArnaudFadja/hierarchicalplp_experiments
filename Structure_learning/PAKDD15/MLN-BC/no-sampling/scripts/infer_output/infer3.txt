args[0] = -aucJarPath
args[2] = -target
args[4] = -trees
args[6] = -model
args[8] = -testNegPosRatio
args[10] = -save
args[11] = -i
args[12] = -test
cmd.getTestDirVal()../data/test3/
test3

% Starting an INFERENCE run of bRDN.
% Running on host: r240n01

% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.

Resetting the LazyGroundNthArgumentClauseIndex.

% Calling ILPouterLoop from createRegressionOuterLooper.

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=0: args[N]=../data/test3/test3_pos.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=1: args[N]=../data/test3/test3_neg.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=2: args[N]=../data/test3/test3_bk.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=3: args[N]=../data/test3/test3_facts.txt

% Welcome to the WILL ILP/SRL systems.


% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.
% Reading background theory from dir: null
% Load '../KDD15_bk.txt'.

% Switching to VarIndicator = uppercase.

% Switching to standard-logic notation for variables; previous setting = uppercase

% Switching to VarIndicator = lowercase.

***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****


***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****

% [ LazyGroundClauseIndex ]  Building full index for mode/1 with 1 assertions.
% LoadAllModes() called.  Currently loaded modes: []

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.
% [ LazyGroundClauseIndex ]  Building full index for sameAs/2 with 2 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for exp/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for log/3.
% LoadAllLibraries() called.  Currently loaded libraries: [listsInLogic, differentInLogic, modes_arithmeticInLogic, inlines_comparisonInLogic, modes_listsInLogic, inlines_differentInLogic, modes_differentInLogic, arithmeticInLogic, inlines_listsInLogic, modes_comparisonInLogic, comparisonInLogic, inlines_arithmeticInLogic]

%  Read the facts.
%  Have read 10,319 facts.
% Have read 684 examples from '../data/test3' [../data/test3/test3*].
% Have read 2,316 examples from '../data/test3' [../data/test3/test3*].

%  LearnOneClause initialized.

% The outer looper has been created.

% Initializing the ILP inner looper.

% NEW target:                 gender(a, b, c)
%  targetPred:                gender/3
%  targetArgTypes:            signature = [Const, Const, Const], types = [+Fold, +Name, +Gender]
%  targets:                   [gender(a, b, c)]
%  targetPredicates:          [gender/3]
%  targetArgSpecs:            [[a[+Fold], b[+Name], c[+Gender]]]
%  variablesInTargets:        [[a, b, c]]

% Started collecting constants

% Collecting the types of constants.

% Looking at the training examples to see if any types of new constants can be inferred.
% Time to collect constants: 2 seconds
% Time to collect examples: 0 seconds

% Read 684 pos examples and 2,316 neg examples.
% Time to init learnOneClause: 2 seconds
% Old dir../data/train3/models/

% Have 684 'raw' positive examples and kept 684.
% Have 2,316 'raw' negative examples and kept 2,316.

% processing backup's for gender
%  POS EX = 684
%  NEG EX = 2,316

% Memory usage by WILLSetup (just counts # targets?):
%  |backupPosExamples| = 1
%  |backupNegExamples| = 1
%  |predicatesAsFacts| = 0
%  |addedToFactBase|   = 0

% Getting bRDN's target predicates.
% Did not learn a model for 'gender' this run.
