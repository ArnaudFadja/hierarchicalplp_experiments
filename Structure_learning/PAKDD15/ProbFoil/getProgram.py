import sys
def extractProgram(Logfilename, ProgramFile):
    file = open(Logfilename, "r")
    file1 = open(ProgramFile, "w")
    for line in iter(file.readline, ''):
        if "RULE LEARNED" in line:
            Rule = line[21:len(line)]
            Parts = Rule.split(":-")
            Head = Parts[0]
            Body = Parts[1]
            index = Body.rfind(' ')
            Prob = Body[index:len(Body)-2]
            RuleNew = Prob + "::"+Head+ ":-"+Body[0:index] + ".\n"
            file1.write(RuleNew)
    else:
        file.close()
        file1.close()


Logfilename = sys.argv[1]
ProgramFile = sys.argv[2]
extractProgram(Logfilename, ProgramFile)
