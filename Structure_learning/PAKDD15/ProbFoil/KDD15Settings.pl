% Modes 
mode(user(+)).
mode(tim(+, -)).
mode(viewed(+, -)).
mode(item(+)).
mode(ic1(+, -)).
mode(ic2(+, -)).
mode(ic2(+, -)).
mode(c1(+)).
mode(c2(+)).
mode(c3(+)).


 % Types definitions 
base(gender(fold, name, gender)).
base(user(fold, name)).
base(viewed(fold, name, id)).
base(ic1(fold, id, idc1)).
base(ic2(fold, id, idc2)).
base(ic3(fold, id, idc3)).
base(c1(fold, idc1)).
base(c2(fold, idc2)).
base(c3(fold, idc3)).



% Target 
learn(gender/3).
 
 % How to generate negative examples
example_mode(auto).
