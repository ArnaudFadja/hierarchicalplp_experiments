#change N with {1..10}

java -cp ../../../../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../../../../Boostr" \
-target gender \
-trees 20 \
-model ../data/train$1/models \
-testNegPosRatio -1 \
-save -i -test ../data/test$1/ \
1>./infer_output/infer$1.txt
