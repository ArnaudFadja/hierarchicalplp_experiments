args[0] = -aucJarPath
args[2] = -target
args[4] = -trees
args[6] = -model
args[8] = -testNegPosRatio
args[10] = -save
args[11] = -i
args[12] = -test
cmd.getTestDirVal()../data/test3/
test3

% Starting an INFERENCE run of bRDN.
% Running on host: r240n01

% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.

Resetting the LazyGroundNthArgumentClauseIndex.

% Calling ILPouterLoop from createRegressionOuterLooper.

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=0: args[N]=../data/test3/test3_pos.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=1: args[N]=../data/test3/test3_neg.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=2: args[N]=../data/test3/test3_bk.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=3: args[N]=../data/test3/test3_facts.txt

% Welcome to the WILL ILP/SRL systems.


% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.
% Reading background theory from dir: null
% Load '../KDD15_bk.txt'.

% Switching to VarIndicator = uppercase.

% Switching to standard-logic notation for variables; previous setting = uppercase

% Switching to VarIndicator = lowercase.

***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****


***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****

% [ LazyGroundClauseIndex ]  Building full index for mode/1 with 1 assertions.
% LoadAllModes() called.  Currently loaded modes: []

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.
% [ LazyGroundClauseIndex ]  Building full index for sameAs/2 with 2 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for exp/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for log/3.
% LoadAllLibraries() called.  Currently loaded libraries: [listsInLogic, differentInLogic, modes_arithmeticInLogic, inlines_comparisonInLogic, modes_listsInLogic, inlines_differentInLogic, modes_differentInLogic, arithmeticInLogic, inlines_listsInLogic, modes_comparisonInLogic, comparisonInLogic, inlines_arithmeticInLogic]

%  Read the facts.
%  Have read 10,319 facts.
% Have read 684 examples from '../data/test3' [../data/test3/test3*].
% Have read 2,316 examples from '../data/test3' [../data/test3/test3*].

%  LearnOneClause initialized.

% The outer looper has been created.

% Initializing the ILP inner looper.

% NEW target:                 gender(a, b, c)
%  targetPred:                gender/3
%  targetArgTypes:            signature = [Const, Const, Const], types = [+Fold, +Name, +Gender]
%  targets:                   [gender(a, b, c)]
%  targetPredicates:          [gender/3]
%  targetArgSpecs:            [[a[+Fold], b[+Name], c[+Gender]]]
%  variablesInTargets:        [[a, b, c]]

% Started collecting constants

% Collecting the types of constants.

% Looking at the training examples to see if any types of new constants can be inferred.
% Time to collect constants: 2 seconds
% Time to collect examples: 0 seconds

% Read 684 pos examples and 2,316 neg examples.
% Time to init learnOneClause: 2 seconds
% Old dir../data/train3/models/

% Have 684 'raw' positive examples and kept 684.
% Have 2,316 'raw' negative examples and kept 2,316.

% processing backup's for gender
%  POS EX = 684
%  NEG EX = 2,316

% Memory usage by WILLSetup (just counts # targets?):
%  |backupPosExamples| = 1
%  |backupNegExamples| = 1
%  |predicatesAsFacts| = 0
%  |addedToFactBase|   = 0

% Getting bRDN's target predicates.
% Did not learn a model for 'gender' this run.
%   loadModel (#0): ../data/train3/models/bRDNs/Trees/genderTree0.tree
%   loadModel (#1): ../data/train3/models/bRDNs/Trees/genderTree1.tree
%   loadModel (#2): ../data/train3/models/bRDNs/Trees/genderTree2.tree
%   loadModel (#3): ../data/train3/models/bRDNs/Trees/genderTree3.tree
%   loadModel (#4): ../data/train3/models/bRDNs/Trees/genderTree4.tree
%   loadModel (#5): ../data/train3/models/bRDNs/Trees/genderTree5.tree
%   loadModel (#6): ../data/train3/models/bRDNs/Trees/genderTree6.tree
%   loadModel (#7): ../data/train3/models/bRDNs/Trees/genderTree7.tree
%   loadModel (#8): ../data/train3/models/bRDNs/Trees/genderTree8.tree
%   loadModel (#9): ../data/train3/models/bRDNs/Trees/genderTree9.tree
%   loadModel (#10): ../data/train3/models/bRDNs/Trees/genderTree10.tree
%   loadModel (#11): ../data/train3/models/bRDNs/Trees/genderTree11.tree
%   loadModel (#12): ../data/train3/models/bRDNs/Trees/genderTree12.tree
%   loadModel (#13): ../data/train3/models/bRDNs/Trees/genderTree13.tree
%   loadModel (#14): ../data/train3/models/bRDNs/Trees/genderTree14.tree
%   loadModel (#15): ../data/train3/models/bRDNs/Trees/genderTree15.tree
%   loadModel (#16): ../data/train3/models/bRDNs/Trees/genderTree16.tree
%   loadModel (#17): ../data/train3/models/bRDNs/Trees/genderTree17.tree
%   loadModel (#18): ../data/train3/models/bRDNs/Trees/genderTree18.tree
%   loadModel (#19): ../data/train3/models/bRDNs/Trees/genderTree19.tree
%  Done loading 20 models.
File: ../data/test3/advice.txt doesnt exist.Hence no advice loaded

% for gender |lookupPos| = 684
% for gender |lookupNeg| = 2,316
% getJointExamples: |pos| = 684, |neg| = 2,316

% Starting inference in bRDN.
% Trees = 20

% Starting getMarginalProbabilities.
% No Gibbs sampling needed during inference.
Best F1 = 0.0
 (Arithmetic) Mean Probability Assigned to Correct Output Class: 2,084.499/3,000.00 = 0.694833

 The weighted count of positive examples = 684.000 and the weighted count of negative examples = 2,316.000

printExamples: Writing out predictions (for Tuffy?) on 3,000 examples for 'gender' to:
  ../data/test3/results_gender.db
 and to:
  ../data/test3/query_gender.db
%    No need to compress since small: ../data/test3/query_gender.db

% Computing Area Under Curves.
%Pos=684
%Neg=2316
%LL:-1335.8366855998252
%LL:-1690.1328315780934

% Running command: java -jar ../../../../Boostr/auc.jar ../data/test3/AUC/aucTemp.txt list 0.0
% WAITING FOR command: java -jar ../../../../Boostr/auc.jar ../data/test3/AUC/aucTemp.txt list 0.0
% DONE WAITING FOR command: java -jar ../../../../Boostr/auc.jar ../data/test3/AUC/aucTemp.txt list 0.0

%   AUC ROC   = 0.500000
%   AUC PR    = 0.228000
%   CLL	      = -0.563378
%   Precision = 0.228000 at threshold = 0.000
%   Recall    = 1.000000
%   F1        = 0.371336

% Total inference time (20 trees): 11.261 seconds.
