useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 2,089.513/3,000.00 = 0.696504
testsetLikelihood(sum(2,089.513), countOfExamples(3,000.00), mean(0.696504)).

// The weighted count of positive examples = 677.000 and the weighted count of negative examples = 2,323.000
weightedSumPos(677.000).
weightedSumNeg(2,323.000).

//  AUC ROC   = 0.500000
//  AUC PR    = 0.225667
//  CLL       = -0.559178
aucROC(gender, 0.500000).
aucPR( gender, 0.225667).

//   Precision = 0.225667 at threshold = 0.000
//   Recall    = 1.000000
//   F1        = 0.368235
precision(gender, 0.225667, usingThreshold(0.0)).
recall(   gender, 1.000000, usingThreshold(0.0)).
F1(       gender, 0.368235, usingThreshold(0.0)).

