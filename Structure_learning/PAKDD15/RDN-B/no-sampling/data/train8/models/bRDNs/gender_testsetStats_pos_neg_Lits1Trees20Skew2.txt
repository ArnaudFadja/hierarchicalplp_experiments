useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 2,058.712/3,000.00 = 0.686237
testsetLikelihood(sum(2,058.712), countOfExamples(3,000.00), mean(0.686237)).

// The weighted count of positive examples = 720.000 and the weighted count of negative examples = 2,280.000
weightedSumPos(720.000).
weightedSumNeg(2,280.000).

//  AUC ROC   = 0.500000
//  AUC PR    = 0.240000
//  CLL       = -0.584978
aucROC(gender, 0.500000).
aucPR( gender, 0.240000).

//   Precision = 0.240000 at threshold = 0.000
//   Recall    = 1.000000
//   F1        = 0.387097
precision(gender, 0.240000, usingThreshold(0.0)).
recall(   gender, 1.000000, usingThreshold(0.0)).
F1(       gender, 0.387097, usingThreshold(0.0)).

