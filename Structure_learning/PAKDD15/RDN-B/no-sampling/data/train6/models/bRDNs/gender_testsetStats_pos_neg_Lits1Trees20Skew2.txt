useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 2,078.769/3,000.00 = 0.692923
testsetLikelihood(sum(2,078.769), countOfExamples(3,000.00), mean(0.692923)).

// The weighted count of positive examples = 692.000 and the weighted count of negative examples = 2,308.000
weightedSumPos(692.000).
weightedSumNeg(2,308.000).

//  AUC ROC   = 0.500000
//  AUC PR    = 0.230667
//  CLL       = -0.568178
aucROC(gender, 0.500000).
aucPR( gender, 0.230667).

//   Precision = 0.230667 at threshold = 0.000
//   Recall    = 1.000000
//   F1        = 0.374865
precision(gender, 0.230667, usingThreshold(0.0)).
recall(   gender, 1.000000, usingThreshold(0.0)).
F1(       gender, 0.374865, usingThreshold(0.0)).

