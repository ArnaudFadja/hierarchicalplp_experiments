useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 2,088.081/3,000.00 = 0.696027
testsetLikelihood(sum(2,088.081), countOfExamples(3,000.00), mean(0.696027)).

// The weighted count of positive examples = 679.000 and the weighted count of negative examples = 2,321.000
weightedSumPos(679.000).
weightedSumNeg(2,321.000).

//  AUC ROC   = 0.500000
//  AUC PR    = 0.226333
//  CLL       = -0.560378
aucROC(gender, 0.500000).
aucPR( gender, 0.226333).

//   Precision = 0.226333 at threshold = 0.000
//   Recall    = 1.000000
//   F1        = 0.369122
precision(gender, 0.226333, usingThreshold(0.0)).
recall(   gender, 1.000000, usingThreshold(0.0)).
F1(       gender, 0.369122, usingThreshold(0.0)).

