#script for training on train_neg.txt and train_pos.txt with RDN-B
#-negPosRatio to avoid subsampling of training negatives 

java -cp ../../../../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target active \
-trees 20 \
-negPosRatio -1 \
-save -l -train ../data/train/ \
1> ris.txt 2>stderr.txt
