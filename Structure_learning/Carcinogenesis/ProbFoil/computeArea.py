from stat import ST_MODE, S_ISREG, S_ISDIR
import os


def getValues(filename):
    file = open(filename, "r")
    Time = 0
    AUCROC = 0
    AUCPR = 0
    for line in iter(file.readline, ''):
        if "Time" in line:
            Time = Time+float(line[5:len(line)-1])
        elif "AUCROC:" in line:
            AUCROC = AUCROC+float(line[8:len(line)-1])
        elif "AUCPR:" in line:
            AUCPR = AUCPR+float(line[7:len(line)-1])
            break
    return Time, AUCROC, AUCPR


def calDir(pathname):
    os.chdir(pathname)
    Output = os.path.join(pathname, "Output/")
    Times = 0
    AUCROCs = 0
    AUCPRs = 0
    i = 0
    for file in os.listdir(Output):
        filename = os.path.join(Output, file)
        Time, AUCROC, AUCPR = getValues(filename)
        Times += Time
        AUCROCs += AUCROC
        AUCPRs += AUCPR
        i = i+1
    if i != 0:
        MedTime = Times/i
        MedROC = AUCROCs/i
        MedPR = AUCPRs/i
    return MedTime, MedROC, MedPR


def Compute(Path):
    for dir in os.listdir(Path):
        pathname = os.path.join(Path, dir)
        mode = os.stat(pathname)[ST_MODE]
        if not S_ISREG(mode):
            MedTime, MedROC, MedPR = calDir(pathname)
            Results = os.path.join(pathname, "Results")
            fileR = open(Results, "w")
            fileR.write("MedTime: ")
            fileR.write(str(MedTime))
            fileR.write("\n")

            fileR.write("MedAUCROC: ")
            fileR.write(str(MedROC))
            fileR.write("\n")

            fileR.write("MedAUCPR: ")
            fileR.write(str(MedPR))
            fileR.write("\n")
            fileR.close()


Path = "/home/arnaud/Documenti/ijar_2019/Experiments/Structure_learning/Probfoil/"
Compute(Path)
