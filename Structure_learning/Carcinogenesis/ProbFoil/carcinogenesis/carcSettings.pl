% Modes 
mode(ames(+,+)).
mode(mutagenic(+,+)).
mode(has_property(+,+,-,-)).
mode(ashby_alert(+,+,-,-)).
mode(ind(+,+,-,-)).
mode(atm(+,+,-,-,-,-)).
mode(symbond(+,+,+,-,-)).
mode(nitro(+,+,-)).
mode(sulfo(+,+,-)).
mode(methyl(+,+,-)).
mode(methoxy(+,+,-)).
mode(amine(+,+,-)).
mode(ketone(+,+,-)).
mode(ether(+,+,-)).
mode(sulfide(+,+,-)).
mode(alcohol(+,+,-)).
mode(phenol(+,+,-)).
mode(ester(+,+,-)).
mode(imine(+,+,-)).
mode(alkyl_halide(+,+,-)).
mode(ar_halide(+,+,-)).
mode(non_ar_6c_ring(+,+,-)).
mode(non_ar_hetero_6_ring(+,+,-)).
mode(six_ring(+,+,-)).
mode(non_ar_5c_ring(+,+,-)).
mode(non_ar_hetero_5_ring(+,+,-)).
mode(five_ring(+,+,-)).
%mode(connected(+,+,+,+)).

 
 % Types definitions 
base(active(fold,model)).
base(ames(fold,model)).
base(mutagenic(fold,model)).
base(has_property(fold,model,property,propval)).
base(ashby_alert(fold,model,alert,ring)).
base(ind(fold,model,alert,nalerts)).
base(atm(fold,model,atomid,element,integer,charge)).
base(symbond(fold,model,atomid,atomid,integer)).
base(nitro(fold,model,ring)).
base(sulfo(fold,model,ring)).
base(methyl(fold,model,ring)).
base(methoxy(fold,model,ring)).
base(amine(fold,model,ring)).
base(ketone(fold,model,ring)).
base(ether(fold,model,ring)).
base(sulfide(fold,model,ring)).
base(alcohol(fold,model,ring)).
base(phenol(fold,model,ring)).
base(ester(fold,model,ring)).
base(imine(fold,model,ring)).
base(alkyl_halide(fold,model,ring)).
base(ar_halide(fold,model,ring)).
base(non_ar_6c_ring(fold,model,ring)).
base(non_ar_hetero_6_ring(fold,model,ring)).
base(six_ring(fold,model,ring)).
base(non_ar_5c_ring(fold,model,ring)).
base(non_ar_hetero_5_ring(fold,model,ring)).
base(five_ring(fold,model,ring)).
%base(connected(fold,model,ring,ring)).

% Target 
learn(active/2).
 
 % How to generate negative examples
example_mode(auto).
