 0.47986577181::active(A,B) :- ames(A,B), imine(A,B,C).
 0.::active(A,B) :- ames(A,B), phenol(A,B,C), ashby_alert(A,B,D,E).
 0.5167785234::active(A,B) :- ames(A,B), five_ring(A,B,C), methyl(A,B,D).
 0.5335570469::active(A,B) :- ar_halide(A,B,C), alcohol(A,B,D), non_ar_6c_ring(A,B,E).
 0.54697986577::active(A,B) :- ames(A,B), non_ar_hetero_5_ring(A,B,C), ashby_alert(A,B,D,E).
 0.55369127516::active(A,B) :- ames(A,B), ind(A,B,C,D), five_ring(A,B,E), nitro(A,B,F).
 0.57046979865::active(A,B) :- ames(A,B), amine(A,B,C), methyl(A,B,D), nitro(A,B,E).
 0.5838926174::active(A,B) :- ames(A,B), ar_halide(A,B,C), methyl(A,B,D).
 0.59395973154::active(A,B) :- ames(A,B), amine(A,B,C), non_ar_6c_ring(A,B,D), ketone(A,B,E).
 0.6006711409::active(A,B) :- non_ar_5c_ring(A,B,C), methyl(A,B,D), ashby_alert(A,B,E,F).
 0.60402684563::active(A,B) :- ar_halide(A,B,C), phenol(A,B,D), ashby_alert(A,B,E,F).
 0.60738255033::active(A,B) :- ames(A,B), ar_halide(A,B,C), nitro(A,B,D).
 0.61073825503::active(A,B) :- ames(A,B), ar_halide(A,B,C), sulfide(A,B,D).
 0.61409395973::active(A,B) :- ames(A,B), amine(A,B,C), non_ar_hetero_6_ring(A,B,D).
 0.6174496644::active(A,B) :- ames(A,B), amine(A,B,C), non_ar_6c_ring(A,B,D), sulfide(A,B,E).
 0.63087248322::active(A,B) :- ames(A,B), amine(A,B,C), non_ar_6c_ring(A,B,D), ether(A,B,E).
 0.65436241610::active(A,B) :- ames(A,B), amine(A,B,C), non_ar_6c_ring(A,B,D).
 0.66442953020::active(A,B) :- ar_halide(A,B,C), ames(A,B).
 0.66778523489::active(A,B) :- ar_halide(A,B,C), phenol(A,B,D).
 0.67785234899::active(A,B) :- alkyl_halide(A,B,C), mutagenic(A,B), ashby_alert(A,B,D,E).
 0.68791946308::active(A,B) :- ester(A,B,C), has_property(A,B,D,E).
 0.69127516778::active(A,B) :- ames(A,B), ind(A,B,C,D), non_ar_hetero_5_ring(A,B,E).
 0.70134228187::active(A,B) :- ar_halide(A,B,C), ames(A,B).
 0.70469798657::active(A,B) :- ames(A,B), methyl(A,B,C), nitro(A,B,D), ketone(A,B,E).
 0.71140939597::active(A,B) :- ar_halide(A,B,C), ether(A,B,D), non_ar_hetero_6_ring(A,B,E), non_ar_6c_ring(A,B,F).
 0.71140939597::active(A,B) :- ar_halide(A,B,C), mutagenic(A,B).
