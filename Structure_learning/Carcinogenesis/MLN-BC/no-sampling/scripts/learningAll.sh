java -cp ../../../../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../../../../Boostr" \
-target active \
-trees 20 \
-negPosRatio -1 \
-save -l -train ../data/train/ \
-mln -mlnClause \
-numMLNClause 3 \
-mlnClauseLen 3 \
1> ris.txt 2>stderr.txt
