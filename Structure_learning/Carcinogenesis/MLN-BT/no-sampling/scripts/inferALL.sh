java -cp ../../../../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../../../../Boostr" \
-target active \
-trees 20 \
-model ../data/train/models \
-testNegPosRatio -1 \
-save -i -test ../data/test/ \
1> infer.txt
