#-negPosRatio avoids subsamping of training negatives

java -cp ../../../../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target active \
-trees 20 \
-negPosRatio -1 \
-save -l -train ../data/train/ \
-mln \
1> ris.txt 2>stderr.txt
