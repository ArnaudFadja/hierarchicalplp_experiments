Time=34.787000 
LL: -95.98355940125056
AUCROC: 1.0
AUCPR: 1.0
Learned Program
active:0.0001582045996608839:-lumo(A).
active:0.0001577433232124242:-bond(B,C,1),hidden_1(B,C).
active:0.0001581398882682275:-benzene(D),hidden_2(D).
active:0.00015810106691745013:-benzene(E),hidden_3(E).
active:0.00015811795661384046:-benzene(F),hidden_4(F).
active:0.00015808564190007511:-carbon_6_ring(G),hidden_5(G).
active:0.0001581430911204795:-nitro(H).
active:0.00015810022190480328:-ball3(I).
active:0.00015811866901704904:-bond(J,K,7),hidden_6(J,K).
active:0.00015814708770245075:-bond(L,M,7),hidden_7(L,M).
active:0.00015815535976539344:-benzene(N),hidden_8(N).
active:0.0001581261897510352:-benzene(O),hidden_9(O).
active:0.00015817176272009604:-benzene(P).
active:0.0001581409140818063:-ring_size_5(Q).
active:0.00015811255602633835:-bond(R,S,7),hidden_11(R,S).
active:0.0001581381375320279:-benzene(T),hidden_12(T).
active:0.00015811312873282405:-benzene(U),hidden_13(U).
active:0.0001581052422496962:-benzene(V),hidden_14(V).
active:0.0001581437202264389:-benzene(W),hidden_15(W).
active:0.00015804886440066662:-carbon_5_aromatic_ring(X),hidden_16(X).
hidden_1(B,C):0.00018903461385564517:-bond(C,Y,1),hidden_1_1(B,C,Y).
hidden_1(B,C):0.00019005377933365195:-bond(Z,B,2),hidden_1_2(C,Z,B).
hidden_1(B,C):0.00019004628711035443:-bond(C,A1,1),hidden_1_3(B,C,A1).
hidden_1(B,C):0.0001901008357756988:-bond(B,B1,1).
hidden_1(B,C):0.0001900814438293554:-atm(B,c,16,C1),hidden_1_4(C,B,C1).
hidden_1(B,C):0.00019004688060965126:-atm(C,c,10,D1),hidden_1_5(B,C,D1).
hidden_2(D):0.00019007210374580262:-ring_size_6(D).
hidden_3(E):0.00019001371971537196:-ring_size_6(E).
hidden_4(F):0.0001900462755065369:-ring_size_6(F).
hidden_5(G):0.00019006373314116822:-ring_size_6(G).
hidden_6(J,K):0.00019000471647140837:-bond(K,E1,7),hidden_6_1(J,K,E1).
hidden_6(J,K):0.00019001738746355674:-bond(F1,J,7).
hidden_6(J,K):0.00019004071733059595:-bond(K,G1,1),hidden_6_4(J,K,G1).
hidden_6(J,K):0.000190051617667215:-atm(J,c,22,H1).
hidden_6(J,K):0.00019000912076889214:-atm(K,c,22,H1),hidden_6_5(J,K,H1).
hidden_7(L,M):0.00019007514987088922:-bond(M,I1,7),hidden_7_1(L,M,I1).
hidden_8(N):0.00019006327558522069:-ring_size_6(N).
hidden_9(O):0.00019004190988679916:-ring_size_6(O).
hidden_11(R,S):0.0001900403233035721:-bond(S,J1,7),hidden_11_1(R,S,J1).
hidden_11(R,S):0.0001900236752862038:-bond(K1,R,7),hidden_11_2(S,K1,R).
hidden_11(R,S):0.00019004292953765246:-bond(R,L1,1),hidden_11_3(S,R,L1).
hidden_11(R,S):0.00019003353102715092:-atm(R,c,22,M1).
hidden_11(R,S):0.00019004279789654543:-atm(S,c,22,M1),hidden_11_4(R,S,M1).
hidden_12(T):0.00019007874434761446:-ring_size_6(T).
hidden_13(U):0.00019001194884699454:-ring_size_6(U).
hidden_14(V):0.00019005373191182558:-ring_size_6(V).
hidden_15(W):0.00019005007266513423:-ring_size_6(W).
hidden_16(X):0.00019001802558466418:-ring_size_5(X).
hidden_1_1(B,C,Y):0.00020973224555940264:-bond(Y,N1,7).
hidden_1_1(B,C,Y):0.0002061791807202375:-bond(Y,O1,7),hidden_1_1_2(B,C,Y,O1).
hidden_1_1(B,C,Y):0.00020970374761018125:-atm(Y,c,22,P1),hidden_1_1_3(B,C,Y,P1).
hidden_1_2(C,Z,B):0.00020977629153448498:-bond(Z,Q1,1).
hidden_1_3(B,C,A1):0.00020982028215443135:-atm(A1,h,3,R1).
hidden_1_4(C,B,C1):0.00020983319231348524:-atm(Z,c,16,C1).
hidden_1_5(B,C,D1):0.00020976613268355838:-atm(B1,h,3,D1).
hidden_6_1(J,K,E1):0.00020977575517047326:-bond(E1,S1,7),hidden_6_1_1(J,K,E1,S1).
hidden_6_1(J,K,E1):0.00020974115223417337:-bond(E1,T1,7),hidden_6_1_2(J,K,E1,T1).
hidden_6_1(J,K,E1):0.00020977004404049935:-atm(E1,c,27,U1),hidden_6_1_3(J,K,E1,U1).
hidden_6_4(J,K,G1):0.00020975906868219902:-atm(G1,h,3,V1),hidden_6_4_1(J,K,G1,V1).
hidden_6_5(J,K,H1):0.00020978558671051121:-atm(F1,c,22,H1).
hidden_7_1(L,M,I1):0.00020979398442142106:-bond(I1,W1,7),hidden_7_1_1(L,M,I1,W1).
hidden_7_1(L,M,I1):0.00020979883046734607:-bond(I1,X1,1).
hidden_11_1(R,S,J1):0.00020981738744725278:-bond(J1,Y1,7).
hidden_11_2(S,K1,R):0.00020980602469862294:-bond(K1,Z1,1).
hidden_11_3(S,R,L1):0.00020981142383785103:-atm(L1,h,3,A2),hidden_11_3_1(S,R,L1,A2).
hidden_11_4(R,S,M1):0.00020977565308418785:-atm(J1,c,22,M1).
hidden_1_1_2(B,C,Y,O1):0.00010457155529402341:-bond(O1,B2,7),hidden_1_1_2_1(B,C,Y,O1,B2).
hidden_1_1_2(B,C,Y,O1):0.0002243058585589558:-bond(O1,C2,1).
hidden_1_1_3(B,C,Y,P1):0.00022494618698982367:-atm(O1,c,22,P1).
hidden_6_1_1(J,K,E1,S1):0.00022501704252947613:-bond(S1,D2,7).
hidden_6_1_2(J,K,E1,T1):0.00022499570791873781:-bond(T1,E2,7),hidden_6_1_2_1(J,K,E1,T1,E2).
hidden_6_1_2(J,K,E1,T1):0.00022498502794425032:-bond(T1,F2,1).
hidden_6_1_3(J,K,E1,U1):0.00022498396705942456:-atm(D2,c,28,U1).
hidden_6_4_1(J,K,G1,V1):0.00022500961259138756:-atm(G2,h,3,V1).
hidden_7_1_1(L,M,I1,W1):0.00022500939289574673:-bond(W1,H2,7),hidden_7_1_1_1(L,M,I1,W1,H2).
hidden_7_1_1(L,M,I1,W1):0.00022501965608624753:-bond(W1,I2,1).
hidden_11_3_1(S,R,L1,A2):0.00022503336001600773:-atm(Z1,h,3,A2).
hidden_1_1_2_1(B,C,Y,O1,B2):0.00010485105238580327:-bond(B2,J2,7),hidden_1_1_2_1_1(B,C,Y,O1,B2,J2).
hidden_6_1_2_1(J,K,E1,T1,E2):0.000237645937884132:-bond(E2,K2,7).
hidden_7_1_1_1(L,M,I1,W1,H2):0.00023765688568653625:-bond(H2,L2,7).
hidden_1_1_2_1_1(B,C,Y,O1,B2,J2):0.00010505997262404037:-bond(J2,M2,7).

 Hyperparameters of muta_deep:

Save statistics? no
Number of arithmetic circuits: 169 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Parameters are initialized with values in the range [-0,50 0,50]
Eta= 0,001000 
Beta1= 0,900000 
Beta2= 0,999000 
Adam_hat= 0,0000000100  
BatchSize= 0 
Strategy= batch 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
