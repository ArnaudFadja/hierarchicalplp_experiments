% Modes 
mode(lumo(+,+,-)).
mode(logp(+,+,-)).
mode(atm(+,+,-,-,-,-)).
mode(bond(+,+,-,-,-)).
mode(benzene(+,+,-)).
mode(carbon_5_aromatic_ring(+,+,-)).
mode(carbon_6_ring(+,+,-)).
mode(hetero_aromatic_6_ring(+,+,-)).
mode(hetero_aromatic_5_ring(+,+,-)).
mode(ring_size_6(+,+,-)).
mode(ring_size_5(+,+,-)).
mode(nitro(+,+,-)).
mode(methyl(+,+,-)).
mode(anthracene(+,+,-)).
mode(phenanthrene(+,+,-)).
mode(ball3(+,+,-)).
%mode(member(+,+,-,+)).
%mode(member(+,+,+,+)).

 
 % Types definitions 
base(active(fold,model)).
base(lumo(fold,model,energy)).
base(logp(fold,model,hydrophob)).
base(atm(fold,model,atomid,element,int,charge)).
base(bond(fold,model,atomid,atomid,int)).
base(benzene(fold,model,ring)).
base(carbon_5_aromatic_ring(fold,model,ring)).
base(carbon_6_ring(fold,model,ring)).
base(hetero_aromatic_6_ring(fold,model,ring)).
base(hetero_aromatic_5_ring(fold,model,ring)).
base(ring_size_6(fold,model,ring)).
base(ring_size_5(fold,model,ring)).
base(nitro(fold,model,ring)).
base(methyl(fold,model,ring)).
base(anthracene(fold,model,ringlist)).
base(phenanthrene(fold,model,ringlist)).
base(ball3(fold,model,ringlist)).
%base(member(fold,model,ring,ringlist)).
%base(member(fold,model,ring,ringlist)).

% Target 
learn(active/2).
 
 % How to generate negative examples
example_mode(auto).
