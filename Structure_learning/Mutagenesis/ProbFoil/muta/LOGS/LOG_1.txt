[INFO] Output level: INFO
[INFO] Random seed: 0.87989534939
[INFO] Computing scores: 11.1085s
[INFO] Number of examples (M): 1521
[INFO] Positive weight (P): 111.0000
[INFO] Negative weight (N): 1410.0000
[INFO] RULE LEARNED: active(A,B) :- ball3(A,B,C) 0.945430637738
[INFO] RULE LEARNED: active(A,B) :- carbon_6_ring(A,B,C) 0.950690335306
[INFO] RULE LEARNED: active(A,B) :- carbon_5_aromatic_ring(A,B,C) 0.955292570677
[INFO] RULE LEARNED: active(A,B) :- phenanthrene(A,B,C) 0.957264957265
[INFO] RULE LEARNED: active(A,B) :- ring_size_5(A,B,C), benzene(A,B,D) 0.973701512163
[INFO] RULE LEARNED: active(A,B) :- anthracene(A,B,C) 0.974358974359
[INFO] RULE LEARNED: active(A,B) :- benzene(A,B,C) 0.962524654832
Time: 1708
