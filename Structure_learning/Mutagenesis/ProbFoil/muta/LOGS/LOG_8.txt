[INFO] Output level: INFO
[INFO] Random seed: 0.947158783625
[INFO] Computing scores: 11.2205s
[INFO] Number of examples (M): 1521
[INFO] Positive weight (P): 117.0000
[INFO] Negative weight (N): 1404.0000
[INFO] RULE LEARNED: active(A,B) :- ball3(A,B,C) 0.940828402367
[INFO] RULE LEARNED: active(A,B) :- carbon_5_aromatic_ring(A,B,C) 0.94674556213
[INFO] RULE LEARNED: active(A,B) :- carbon_6_ring(A,B,C) 0.952662721893
[INFO] RULE LEARNED: active(A,B) :- ring_size_5(A,B,C) 0.974358974359
[INFO] RULE LEARNED: active(A,B) :- phenanthrene(A,B,C) 0.975673898751
[INFO] RULE LEARNED: active(A,B) :- anthracene(A,B,C) 0.976331360947
[INFO] RULE LEARNED: active(A,B) :- lumo(A,B,C) 0.965811965812
Time: 1905
