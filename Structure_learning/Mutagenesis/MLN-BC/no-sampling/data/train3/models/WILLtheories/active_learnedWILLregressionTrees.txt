useStdLogicNotation: true.

% maxTreeDepthInNodes                 = 5
% maxTreeDepthInLiterals              = 12
% maxNumberOfLiteralsAtAnInteriorNode = 1
% maxFreeBridgersInBody               = 1
% maxNumberOfClauses                  = 3
% maxNodesToConsider                  = 10
% maxNodesToCreate                    = 10,000
% maxAcceptableNodeScoreToStop        = 0.003
% negPosRatio                         = -1.000
% testNegPosRatio                     = -1.000
% # of pos examples                   = 169
% # of neg examples                   = 0




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%  Final call for computing score for active.  %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

stepLength_tree1(1.0).
stepLength_tree2(1.0).
stepLength_tree3(1.0).
stepLength_tree4(1.0).
stepLength_tree5(1.0).
stepLength_tree6(1.0).
stepLength_tree7(1.0).
stepLength_tree8(1.0).
stepLength_tree9(1.0).
stepLength_tree10(1.0).
stepLength_tree11(1.0).
stepLength_tree12(1.0).
stepLength_tree13(1.0).
stepLength_tree14(1.0).
stepLength_tree15(1.0).
stepLength_tree16(1.0).
stepLength_tree17(1.0).
stepLength_tree18(1.0).
stepLength_tree19(1.0).
stepLength_tree20(1.0).

logPrior(-1.8).
active(a, total) :- // A general accessor. 
   active(a, 1000000, total), !.
active(a, total) :- waitHere("This should not fail", active(a, total)).

active(a, treesToUse, total) :- // A tree-limited accessor (e.g., for tuning the number of trees to use).
   logPrior(logPrior),
   getScore_active_tree1(a, treesToUse, total1),
   getScore_active_tree2(a, treesToUse, total2),
   getScore_active_tree3(a, treesToUse, total3),
   getScore_active_tree4(a, treesToUse, total4),
   getScore_active_tree5(a, treesToUse, total5),
   getScore_active_tree6(a, treesToUse, total6),
   getScore_active_tree7(a, treesToUse, total7),
   getScore_active_tree8(a, treesToUse, total8),
   getScore_active_tree9(a, treesToUse, total9),
   getScore_active_tree10(a, treesToUse, total10),
   getScore_active_tree11(a, treesToUse, total11),
   getScore_active_tree12(a, treesToUse, total12),
   getScore_active_tree13(a, treesToUse, total13),
   getScore_active_tree14(a, treesToUse, total14),
   getScore_active_tree15(a, treesToUse, total15),
   getScore_active_tree16(a, treesToUse, total16),
   getScore_active_tree17(a, treesToUse, total17),
   getScore_active_tree18(a, treesToUse, total18),
   getScore_active_tree19(a, treesToUse, total19),
   getScore_active_tree20(a, treesToUse, total20),
   total is logPrior + total1 + total2 + total3 + total4 + total5 + total6 + total7 + total8 + total9 + total10 + total11 + total12 + total13 + total14 + total15 + total16 + total17 + total18 + total19 + total20,
   !.
active(a, treesToUse, total) :- waitHere("This should not fail", active(a, treesToUse, total)).

getScore_active_tree1(a, treesToUse, 0.0) :- 1 > treesToUse, !.
getScore_active_tree1(a, treesToUse, total1) :- active_tree1(a, total), stepLength_tree1(stepLen), total1 is total * stepLen.

getScore_active_tree2(a, treesToUse, 0.0) :- 2 > treesToUse, !.
getScore_active_tree2(a, treesToUse, total2) :- active_tree2(a, total), stepLength_tree2(stepLen), total2 is total * stepLen.

getScore_active_tree3(a, treesToUse, 0.0) :- 3 > treesToUse, !.
getScore_active_tree3(a, treesToUse, total3) :- active_tree3(a, total), stepLength_tree3(stepLen), total3 is total * stepLen.

getScore_active_tree4(a, treesToUse, 0.0) :- 4 > treesToUse, !.
getScore_active_tree4(a, treesToUse, total4) :- active_tree4(a, total), stepLength_tree4(stepLen), total4 is total * stepLen.

getScore_active_tree5(a, treesToUse, 0.0) :- 5 > treesToUse, !.
getScore_active_tree5(a, treesToUse, total5) :- active_tree5(a, total), stepLength_tree5(stepLen), total5 is total * stepLen.

getScore_active_tree6(a, treesToUse, 0.0) :- 6 > treesToUse, !.
getScore_active_tree6(a, treesToUse, total6) :- active_tree6(a, total), stepLength_tree6(stepLen), total6 is total * stepLen.

getScore_active_tree7(a, treesToUse, 0.0) :- 7 > treesToUse, !.
getScore_active_tree7(a, treesToUse, total7) :- active_tree7(a, total), stepLength_tree7(stepLen), total7 is total * stepLen.

getScore_active_tree8(a, treesToUse, 0.0) :- 8 > treesToUse, !.
getScore_active_tree8(a, treesToUse, total8) :- active_tree8(a, total), stepLength_tree8(stepLen), total8 is total * stepLen.

getScore_active_tree9(a, treesToUse, 0.0) :- 9 > treesToUse, !.
getScore_active_tree9(a, treesToUse, total9) :- active_tree9(a, total), stepLength_tree9(stepLen), total9 is total * stepLen.

getScore_active_tree10(a, treesToUse, 0.0) :- 10 > treesToUse, !.
getScore_active_tree10(a, treesToUse, total10) :- active_tree10(a, total), stepLength_tree10(stepLen), total10 is total * stepLen.

getScore_active_tree11(a, treesToUse, 0.0) :- 11 > treesToUse, !.
getScore_active_tree11(a, treesToUse, total11) :- active_tree11(a, total), stepLength_tree11(stepLen), total11 is total * stepLen.

getScore_active_tree12(a, treesToUse, 0.0) :- 12 > treesToUse, !.
getScore_active_tree12(a, treesToUse, total12) :- active_tree12(a, total), stepLength_tree12(stepLen), total12 is total * stepLen.

getScore_active_tree13(a, treesToUse, 0.0) :- 13 > treesToUse, !.
getScore_active_tree13(a, treesToUse, total13) :- active_tree13(a, total), stepLength_tree13(stepLen), total13 is total * stepLen.

getScore_active_tree14(a, treesToUse, 0.0) :- 14 > treesToUse, !.
getScore_active_tree14(a, treesToUse, total14) :- active_tree14(a, total), stepLength_tree14(stepLen), total14 is total * stepLen.

getScore_active_tree15(a, treesToUse, 0.0) :- 15 > treesToUse, !.
getScore_active_tree15(a, treesToUse, total15) :- active_tree15(a, total), stepLength_tree15(stepLen), total15 is total * stepLen.

getScore_active_tree16(a, treesToUse, 0.0) :- 16 > treesToUse, !.
getScore_active_tree16(a, treesToUse, total16) :- active_tree16(a, total), stepLength_tree16(stepLen), total16 is total * stepLen.

getScore_active_tree17(a, treesToUse, 0.0) :- 17 > treesToUse, !.
getScore_active_tree17(a, treesToUse, total17) :- active_tree17(a, total), stepLength_tree17(stepLen), total17 is total * stepLen.

getScore_active_tree18(a, treesToUse, 0.0) :- 18 > treesToUse, !.
getScore_active_tree18(a, treesToUse, total18) :- active_tree18(a, total), stepLength_tree18(stepLen), total18 is total * stepLen.

getScore_active_tree19(a, treesToUse, 0.0) :- 19 > treesToUse, !.
getScore_active_tree19(a, treesToUse, total19) :- active_tree19(a, total), stepLength_tree19(stepLen), total19 is total * stepLen.

getScore_active_tree20(a, treesToUse, 0.0) :- 20 > treesToUse, !.
getScore_active_tree20(a, treesToUse, total20) :- active_tree20(a, total), stepLength_tree20(stepLen), total20 is total * stepLen.

flattenedLiteralsInThisSetOfTrees(0, []).
