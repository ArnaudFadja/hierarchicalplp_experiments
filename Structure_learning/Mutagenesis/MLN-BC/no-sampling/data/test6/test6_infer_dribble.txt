% Running on host: r240n01

% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.

% Calling ILPouterLoop from createRegressionOuterLooper.

% getInputArgWithDefaultValue: args=[../data/test6/test6_pos.txt, ../data/test6/test6_neg.txt, ../data/test6/test6_bk.txt, ../data/test6/test6_facts.txt]
%  for N=0: args[N]=../data/test6/test6_pos.txt

% getInputArgWithDefaultValue: args=[../data/test6/test6_pos.txt, ../data/test6/test6_neg.txt, ../data/test6/test6_bk.txt, ../data/test6/test6_facts.txt]
%  for N=1: args[N]=../data/test6/test6_neg.txt

% getInputArgWithDefaultValue: args=[../data/test6/test6_pos.txt, ../data/test6/test6_neg.txt, ../data/test6/test6_bk.txt, ../data/test6/test6_facts.txt]
%  for N=2: args[N]=../data/test6/test6_bk.txt

% getInputArgWithDefaultValue: args=[../data/test6/test6_pos.txt, ../data/test6/test6_neg.txt, ../data/test6/test6_bk.txt, ../data/test6/test6_facts.txt]
%  for N=3: args[N]=../data/test6/test6_facts.txt

% Welcome to the WILL ILP/SRL systems.


% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.
% Reading background theory from dir: null
% Load '../mutagenesis_bk.txt'.

% Switching to VarIndicator = uppercase.

% Switching to standard-logic notation for variables; previous setting = uppercase

% Switching to VarIndicator = lowercase.

***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****


***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****

% [ LazyGroundClauseIndex ]  Building full index for mode/1 with 1 assertions.
% LoadAllModes() called.  Currently loaded modes: []

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.
% [ LazyGroundClauseIndex ]  Building full index for sameAs/2 with 2 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for exp/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for log/3.
% LoadAllLibraries() called.  Currently loaded libraries: [listsInLogic, differentInLogic, modes_arithmeticInLogic, inlines_comparisonInLogic, modes_listsInLogic, inlines_differentInLogic, modes_differentInLogic, arithmeticInLogic, inlines_listsInLogic, modes_comparisonInLogic, comparisonInLogic, inlines_arithmeticInLogic]

%  Read the facts.
%  Have read 15,065 facts.
% Have read 14 examples from '../data/test6' [../data/test6/test6*].
% Have read 5 examples from '../data/test6' [../data/test6/test6*].

%  LearnOneClause initialized.

% The outer looper has been created.

% Initializing the ILP inner looper.

% NEW target:                 active(a)
%  targetPred:                active/1
%  targetArgTypes:            signature = [Const], types = [+Drug]
%  targets:                   [active(a)]
%  targetPredicates:          [active/1]
%  targetArgSpecs:            [[a[+Drug]]]
%  variablesInTargets:        [[a]]

% Started collecting constants

% Collecting the types of constants.

%   *** WARNING ***  Constant '8' is already marked as being of types = [Atmtype];
%          type = 'Charge' may be added if not already known.
%  PredicateName = 'atm', from 'atm(D108, D108_3, C, 27, 8)',
%  which has types = [signature = [Const, Const, Const, Const, Const], types = [+Drug, -Atomid, #Element, #Atmtype, -Charge]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '94' is already marked as being of types = [Charge];
%          type = 'Atmtype' may be added if not already known.
%  PredicateName = 'atm', from 'atm(D160, D160_12, BR, 94, -124)',
%  which has types = [signature = [Const, Const, Const, Const, Const], types = [+Drug, -Atomid, #Element, #Atmtype, -Charge]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '7' is already marked as being of types = [Charge];
%          type = 'Bondtype' may be added if not already known.
%  PredicateName = 'bond', from 'bond(D1, D1_1, D1_2, 7)',
%  which has types = [signature = [Const, Const, Const, Const], types = [+Drug, -Atomid, -Atomid, #Bondtype]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

% Looking at the training examples to see if any types of new constants can be inferred.
% Time to collect constants: 6 seconds
% Time to collect examples: 0 seconds

% Read 14 pos examples and 5 neg examples.
% Time to init learnOneClause: 6 seconds
% Old dir../data/train6/models/

% Have 14 'raw' positive examples and kept 14.
% Have 5 'raw' negative examples and kept 5.

% processing backup's for active
%  POS EX = 14
%  NEG EX = 5

% Memory usage by WILLSetup (just counts # targets?):
%  |backupPosExamples| = 1
%  |backupNegExamples| = 1
%  |predicatesAsFacts| = 0
%  |addedToFactBase|   = 0

% Getting bRDN's target predicates.
% Did not learn a model for 'active' this run.
%   loadModel (#0): ../data/train6/models/bRDNs/Trees/activeTree0.tree
%   loadModel (#1): ../data/train6/models/bRDNs/Trees/activeTree1.tree
%   loadModel (#2): ../data/train6/models/bRDNs/Trees/activeTree2.tree
%   loadModel (#3): ../data/train6/models/bRDNs/Trees/activeTree3.tree
%   loadModel (#4): ../data/train6/models/bRDNs/Trees/activeTree4.tree
%   loadModel (#5): ../data/train6/models/bRDNs/Trees/activeTree5.tree
%   loadModel (#6): ../data/train6/models/bRDNs/Trees/activeTree6.tree
%   loadModel (#7): ../data/train6/models/bRDNs/Trees/activeTree7.tree
%   loadModel (#8): ../data/train6/models/bRDNs/Trees/activeTree8.tree
%   loadModel (#9): ../data/train6/models/bRDNs/Trees/activeTree9.tree
%   loadModel (#10): ../data/train6/models/bRDNs/Trees/activeTree10.tree
%   loadModel (#11): ../data/train6/models/bRDNs/Trees/activeTree11.tree
%   loadModel (#12): ../data/train6/models/bRDNs/Trees/activeTree12.tree
%   loadModel (#13): ../data/train6/models/bRDNs/Trees/activeTree13.tree
%   loadModel (#14): ../data/train6/models/bRDNs/Trees/activeTree14.tree
%   loadModel (#15): ../data/train6/models/bRDNs/Trees/activeTree15.tree
%   loadModel (#16): ../data/train6/models/bRDNs/Trees/activeTree16.tree
%   loadModel (#17): ../data/train6/models/bRDNs/Trees/activeTree17.tree
%   loadModel (#18): ../data/train6/models/bRDNs/Trees/activeTree18.tree
%   loadModel (#19): ../data/train6/models/bRDNs/Trees/activeTree19.tree
%  Done loading 20 models.
File: ../data/test6/advice.txt doesnt exist.Hence no advice loaded

% for active |lookupPos| = 14
% for active |lookupNeg| = 5
% getJointExamples: |pos| = 14, |neg| = 5

% Starting inference in bRDN.
% Trees = 20

% Starting getMarginalProbabilities.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for methyl/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for ring_size_5/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for atm/5.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for bond/4.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for benzene/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for nitro/2.
% No Gibbs sampling needed during inference.
 (Arithmetic) Mean Probability Assigned to Correct Output Class: 6.293/19.00 = 0.331217

 The weighted count of positive examples = 14.000 and the weighted count of negative examples = 5.000

printExamples: Writing out predictions (for Tuffy?) on 19 examples for 'active' to:
  ../data/test6/results_active.db
 and to:
  ../data/test6/query_active.db
%    No need to compress since small: ../data/test6/query_active.db

% Computing Area Under Curves.
%Pos=14
%Neg=5
%LL:-27.158673858729102
%LL:-27.935681612113328

% Running command: java -jar ../../../../Boostr//auc.jar ../data/test6/AUC/aucTemp.txt list 0.0
% WAITING FOR command: java -jar ../../../../Boostr//auc.jar ../data/test6/AUC/aucTemp.txt list 0.0
% DONE WAITING FOR command: java -jar ../../../../Boostr//auc.jar ../data/test6/AUC/aucTemp.txt list 0.0

%   AUC ROC   = 0.500000
%   AUC PR    = 0.727497
%   CLL	      = -1.470299
%   Precision = 0.750000 at threshold = 0.147
%   Recall    = 0.214286
%   F1        = 0.333333

% Total inference time (20 trees): 15.544 seconds.
