java -cp ../../../../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
	-aucJarPath "../../../../Boostr/" \
	-target active \
	-trees 20 \
	-negPosRatio -1 \
	-save -l -train ../data/train$1/ \
	-mln -mlnClause \
	-numMLNClause 3 \
	-mlnClauseLen 3 \
	1> ./learn_output/ris$1.txt 2>./learn_output/stderr$1.txt
