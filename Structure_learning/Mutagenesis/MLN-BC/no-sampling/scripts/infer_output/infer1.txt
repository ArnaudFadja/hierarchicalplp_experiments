args[0] = -aucJarPath
args[2] = -target
args[4] = -trees
args[6] = -model
args[8] = -testNegPosRatio
args[10] = -save
args[11] = -i
args[12] = -test
cmd.getTestDirVal()../data/test1/
test1

% Starting an INFERENCE run of bRDN.
% Running on host: r240n01

% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.

Resetting the LazyGroundNthArgumentClauseIndex.

% Calling ILPouterLoop from createRegressionOuterLooper.

% getInputArgWithDefaultValue: args=[../data/test1/test1_pos.txt, ../data/test1/test1_neg.txt, ../data/test1/test1_bk.txt, ../data/test1/test1_facts.txt]
%  for N=0: args[N]=../data/test1/test1_pos.txt

% getInputArgWithDefaultValue: args=[../data/test1/test1_pos.txt, ../data/test1/test1_neg.txt, ../data/test1/test1_bk.txt, ../data/test1/test1_facts.txt]
%  for N=1: args[N]=../data/test1/test1_neg.txt

% getInputArgWithDefaultValue: args=[../data/test1/test1_pos.txt, ../data/test1/test1_neg.txt, ../data/test1/test1_bk.txt, ../data/test1/test1_facts.txt]
%  for N=2: args[N]=../data/test1/test1_bk.txt

% getInputArgWithDefaultValue: args=[../data/test1/test1_pos.txt, ../data/test1/test1_neg.txt, ../data/test1/test1_bk.txt, ../data/test1/test1_facts.txt]
%  for N=3: args[N]=../data/test1/test1_facts.txt

% Welcome to the WILL ILP/SRL systems.


% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.
% Reading background theory from dir: null
% Load '../mutagenesis_bk.txt'.

% Switching to VarIndicator = uppercase.

% Switching to standard-logic notation for variables; previous setting = uppercase

% Switching to VarIndicator = lowercase.

***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****


***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****

% [ LazyGroundClauseIndex ]  Building full index for mode/1 with 1 assertions.
% LoadAllModes() called.  Currently loaded modes: []

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.
% [ LazyGroundClauseIndex ]  Building full index for sameAs/2 with 2 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for exp/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for log/3.
% LoadAllLibraries() called.  Currently loaded libraries: [listsInLogic, differentInLogic, modes_arithmeticInLogic, inlines_comparisonInLogic, modes_listsInLogic, inlines_differentInLogic, modes_differentInLogic, arithmeticInLogic, inlines_listsInLogic, modes_comparisonInLogic, comparisonInLogic, inlines_arithmeticInLogic]

%  Read the facts.
%  Have read 15,065 facts.
% Have read 14 examples from '../data/test1' [../data/test1/test1*].
% Have read 5 examples from '../data/test1' [../data/test1/test1*].

%  LearnOneClause initialized.

% The outer looper has been created.

% Initializing the ILP inner looper.

% NEW target:                 active(a)
%  targetPred:                active/1
%  targetArgTypes:            signature = [Const], types = [+Drug]
%  targets:                   [active(a)]
%  targetPredicates:          [active/1]
%  targetArgSpecs:            [[a[+Drug]]]
%  variablesInTargets:        [[a]]

% Started collecting constants

% Collecting the types of constants.

%   *** WARNING ***  Constant '8' is already marked as being of types = [Atmtype];
%          type = 'Charge' may be added if not already known.
%  PredicateName = 'atm', from 'atm(D108, D108_3, C, 27, 8)',
%  which has types = [signature = [Const, Const, Const, Const, Const], types = [+Drug, -Atomid, #Element, #Atmtype, -Charge]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '94' is already marked as being of types = [Charge];
%          type = 'Atmtype' may be added if not already known.
%  PredicateName = 'atm', from 'atm(D160, D160_12, BR, 94, -124)',
%  which has types = [signature = [Const, Const, Const, Const, Const], types = [+Drug, -Atomid, #Element, #Atmtype, -Charge]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '7' is already marked as being of types = [Charge];
%          type = 'Bondtype' may be added if not already known.
%  PredicateName = 'bond', from 'bond(D1, D1_1, D1_2, 7)',
%  which has types = [signature = [Const, Const, Const, Const], types = [+Drug, -Atomid, -Atomid, #Bondtype]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

% Looking at the training examples to see if any types of new constants can be inferred.
% Time to collect constants: 5 seconds
% Time to collect examples: 0 seconds

% Read 14 pos examples and 5 neg examples.
% Time to init learnOneClause: 5 seconds
% Old dir../data/train1/models/

% Have 14 'raw' positive examples and kept 14.
% Have 5 'raw' negative examples and kept 5.

% processing backup's for active
%  POS EX = 14
%  NEG EX = 5

% Memory usage by WILLSetup (just counts # targets?):
%  |backupPosExamples| = 1
%  |backupNegExamples| = 1
%  |predicatesAsFacts| = 0
%  |addedToFactBase|   = 0

% Getting bRDN's target predicates.
% Did not learn a model for 'active' this run.
%   loadModel (#0): ../data/train1/models/bRDNs/Trees/activeTree0.tree
%   loadModel (#1): ../data/train1/models/bRDNs/Trees/activeTree1.tree
%   loadModel (#2): ../data/train1/models/bRDNs/Trees/activeTree2.tree
%   loadModel (#3): ../data/train1/models/bRDNs/Trees/activeTree3.tree
%   loadModel (#4): ../data/train1/models/bRDNs/Trees/activeTree4.tree
%   loadModel (#5): ../data/train1/models/bRDNs/Trees/activeTree5.tree
%   loadModel (#6): ../data/train1/models/bRDNs/Trees/activeTree6.tree
%   loadModel (#7): ../data/train1/models/bRDNs/Trees/activeTree7.tree
%   loadModel (#8): ../data/train1/models/bRDNs/Trees/activeTree8.tree
%   loadModel (#9): ../data/train1/models/bRDNs/Trees/activeTree9.tree
%   loadModel (#10): ../data/train1/models/bRDNs/Trees/activeTree10.tree
%   loadModel (#11): ../data/train1/models/bRDNs/Trees/activeTree11.tree
%   loadModel (#12): ../data/train1/models/bRDNs/Trees/activeTree12.tree
%   loadModel (#13): ../data/train1/models/bRDNs/Trees/activeTree13.tree
%   loadModel (#14): ../data/train1/models/bRDNs/Trees/activeTree14.tree
%   loadModel (#15): ../data/train1/models/bRDNs/Trees/activeTree15.tree
%   loadModel (#16): ../data/train1/models/bRDNs/Trees/activeTree16.tree
%   loadModel (#17): ../data/train1/models/bRDNs/Trees/activeTree17.tree
%   loadModel (#18): ../data/train1/models/bRDNs/Trees/activeTree18.tree
%   loadModel (#19): ../data/train1/models/bRDNs/Trees/activeTree19.tree
%  Done loading 20 models.
File: ../data/test1/advice.txt doesnt exist.Hence no advice loaded

% for active |lookupPos| = 14
% for active |lookupNeg| = 5
% getJointExamples: |pos| = 14, |neg| = 5

% Starting inference in bRDN.
% Trees = 20

% Starting getMarginalProbabilities.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for methyl/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for benzene/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for bond/4.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for atm/5.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for nitro/2.
% No Gibbs sampling needed during inference.
Best F1 = 0.923076923076923
 (Arithmetic) Mean Probability Assigned to Correct Output Class: 6.300/19.00 = 0.331599

 The weighted count of positive examples = 14.000 and the weighted count of negative examples = 5.000

printExamples: Writing out predictions (for Tuffy?) on 19 examples for 'active' to:
  ../data/test1/results_active.db
 and to:
  ../data/test1/query_active.db
%    No need to compress since small: ../data/test1/query_active.db

% Computing Area Under Curves.
%Pos=14
%Neg=5
%LL:-27.187093934450512
%LL:-27.95198198708089

% Running command: java -jar ../../../../Boostr//auc.jar ../data/test1/AUC/aucTemp.txt list 0.0
% WAITING FOR command: java -jar ../../../../Boostr//auc.jar ../data/test1/AUC/aucTemp.txt list 0.0
% DONE WAITING FOR command: java -jar ../../../../Boostr//auc.jar ../data/test1/AUC/aucTemp.txt list 0.0

%   AUC ROC   = 0.535714
%   AUC PR    = 0.786269
%   CLL	      = -1.471157
%   Precision = 1.000000 at threshold = 0.154
%   Recall    = 0.071429
%   F1        = 0.133333

% Total inference time (20 trees): 15.575 seconds.
