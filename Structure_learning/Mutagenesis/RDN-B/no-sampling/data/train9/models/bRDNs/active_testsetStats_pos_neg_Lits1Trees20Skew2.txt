useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 15.788/19.00 = 0.830972
testsetLikelihood(sum(15.788), countOfExamples(19.00), mean(0.830972)).

// The weighted count of positive examples = 14.000 and the weighted count of negative examples = 5.000
weightedSumPos(14.000).
weightedSumNeg(5.000).

//  AUC ROC   = 1.000000
//  AUC PR    = 1.000000
//  CLL       = -0.199717
aucROC(active, 1.000000).
aucPR( active, 1.000000).

//   Precision = 1.000000 at threshold = 0.521
//   Recall    = 1.000000
//   F1        = 1.000000
precision(active, 1.000000, usingThreshold(0.520732544468012)).
recall(   active, 1.000000, usingThreshold(0.520732544468012)).
F1(       active, 1.000000, usingThreshold(0.520732544468012)).

