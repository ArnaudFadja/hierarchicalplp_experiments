Time=6.812000 
LL: -7.388518539128808
AUCROC: 0.9571428571428572
AUCPR: 0.9854755363683936
Learned Program
active:0.09143339086784044:-bond(A,B,7),hidden_1(A,B).
active:0.2877466218765135:-bond(C,D,7),hidden_2(C,D).
active:2.7111403024858476e-5:-benzene(E),hidden_3(E).
active:0.00032976444564903116:-benzene(F),hidden_4(F).
active:0.7623081139526106:-bond(G,H,7),hidden_5(G,H).
active:0.0009090027186391943:-benzene(I),hidden_6(I).
active:0.0008700912655349214:-benzene(J),hidden_9(J).
active:0.007538359566999178:-benzene(K),hidden_10(K).
active:0.0008690168750681817:-benzene(L),hidden_11(L).
active:0.00011091275236086745:-benzene(M),hidden_12(M).
hidden_1(A,B):0.04051182455338859:-bond(B,N,7),hidden_1_1(A,B,N).
hidden_1(A,B):0.0257317247834762:-bond(A,O,1),hidden_1_2(B,A,O).
hidden_1(A,B):0.02948119363715512:-atm(A,c,22,P).
hidden_2(C,D):0.01811671459693601:-bond(D,Q,7),hidden_2_1(C,D,Q).
hidden_2(C,D):0.006808181868613741:-bond(R,C,7).
hidden_3(E):0.001436306484066353:-ring_size_6(E).
hidden_4(F):0.006880091296636447:-ring_size_6(F).
hidden_5(G,H):0.3968196838193949:-bond(H,S,7),hidden_5_1(G,H,S).
hidden_5(G,H):0.0044733551435756445:-bond(T,G,7).
hidden_5(G,H):0.01024506179085218:-bond(G,U,1),hidden_5_3(H,G,U).
hidden_5(G,H):0.2070394980017057:-bond(H,V,1),hidden_5_4(G,H,V).
hidden_5(G,H):0.00020209452081020778:-atm(G,c,22,W).
hidden_5(G,H):0.30775291647281183:-atm(H,c,22,W),hidden_5_5(G,H,W).
hidden_6(I):0.0012026709195002924:-ring_size_6(I).
hidden_9(J):0.004511863727928076:-ring_size_6(J).
hidden_10(K):0.0002704572237198433:-ring_size_6(K).
hidden_11(L):0.006363318855155279:-ring_size_6(L).
hidden_12(M):0.0030296655121209476:-ring_size_6(M).
hidden_1_1(A,B,N):0.23819132571691515:-bond(N,X,7),hidden_1_1_1(A,B,N,X).
hidden_1_1(A,B,N):0.022199631401740492:-bond(N,Y,1),hidden_1_1_2(A,B,N,Y).
hidden_1_2(B,A,O):0.005440894104061014:-bond(O,Z,2),hidden_1_2_2(B,A,O,Z).
hidden_1_2(B,A,O):6.755769055247365e-5:-atm(O,n,38,A1).
hidden_2_1(C,D,Q):0.27495009062055936:-bond(Q,B1,7),hidden_2_1_1(C,D,Q,B1).
hidden_5_1(G,H,S):0.3861068247534044:-bond(S,C1,7),hidden_5_1_1(G,H,S,C1).
hidden_5_1(G,H,S):0.011041527843350838:-bond(S,D1,1),hidden_5_1_2(G,H,S,D1).
hidden_5_3(H,G,U):0.10515224264784241:-atm(U,h,3,E1).
hidden_5_4(G,H,V):0.20239313049282268:-atm(V,h,3,E1),hidden_5_4_1(G,H,V,E1).
hidden_5_5(G,H,W):0.052733853364795504:-atm(S,c,22,W).
hidden_1_1_1(A,B,N,X):0.3360083797527792:-bond(X,F1,7),hidden_1_1_1_1(A,B,N,X,F1).
hidden_1_1_1(A,B,N,X):0.2847065709217925:-bond(X,R,1).
hidden_1_1_2(A,B,N,Y):0.001139891314494361:-bond(Y,G1,1).
hidden_1_2_2(B,A,O,Z):9.350299089079871e-5:-atm(Z,o,40,H1).
hidden_2_1_1(C,D,Q,B1):0.2133124520406935:-bond(B1,I1,1).
hidden_5_1_1(G,H,S,C1):0.014362090703637911:-bond(C1,J1,7).
hidden_5_1_2(G,H,S,D1):0.005005540351211337:-bond(D1,K1,2).
hidden_5_1_2(G,H,S,D1):0.00015519245200865944:-bond(D1,L1,2),hidden_5_1_2_2(G,H,S,D1,L1).
hidden_5_1_2(G,H,S,D1):0.00012395454997297397:-atm(D1,n,38,M1).
hidden_5_4_1(G,H,V,E1):0.142952568127337:-atm(N1,h,3,E1).
hidden_1_1_1_1(A,B,N,X,F1):0.07045061771277202:-bond(F1,O1,1).
hidden_5_1_2_2(G,H,S,D1,L1):0.006861865956357175:-atm(L1,o,40,P1).

 Hyperparameters of muta_deep:

Save statistics? no
Number of arithmetic circuits: 169 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Regularization enabled
Regularization type L3:
a:0,000000
fraction of examples b:0,200000
