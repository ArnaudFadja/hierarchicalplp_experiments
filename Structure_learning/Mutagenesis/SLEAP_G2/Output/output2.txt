Time=49.691000 
LL: -29.822718770195692
AUCROC: 0.9333333333333333
AUCPR: 0.9539682539682539
Learned Program
active:0.00731630359006599:-lumo(A).
active:0.007310255860814177:-bond(B,C,7),hidden_1(B,C).
active:0.007341908940797439:-bond(D,E,7),hidden_2(D,E).
active:0.00736974064221387:-benzene(F),hidden_3(F).
active:0.007372763593558252:-benzene(G),hidden_4(G).
active:0.007381216883195306:-benzene(H),hidden_5(H).
active:0.009129794389290935:-ring_size_5(I).
active:0.007298251965631229:-nitro(J).
active:0.007298834630254768:-bond(K,L,7),hidden_6(K,L).
active:0.0073759932658615706:-benzene(M),hidden_7(M).
active:0.0073820343290082946:-benzene(N),hidden_8(N).
active:0.007374249105401805:-benzene(O),hidden_9(O).
active:0.007392214103622794:-benzene(P),hidden_10(P).
active:0.0073794925899600405:-benzene(Q),hidden_11(Q).
active:0.010872550047322436:-anthracene(R).
active:0.010873260760484942:-phenanthrene(S).
active:0.010872771555797038:-ball3(T).
active:0.0073070080465916735:-bond(U,V,7),hidden_12(U,V).
active:0.007378190417395961:-benzene(W),hidden_13(W).
hidden_1(B,C):0.010753160053869799:-bond(C,X,7),hidden_1_1(B,C,X).
hidden_1(B,C):0.010179059262236124:-bond(Y,B,7),hidden_1_2(C,Y,B).
hidden_1(B,C):0.010405286706068202:-bond(B,Z,1).
hidden_1(B,C):0.009928662069247803:-bond(C,A1,1),hidden_1_4(B,C,A1).
hidden_1(B,C):0.010681380880589054:-atm(B,c,22,B1).
hidden_1(B,C):0.01009405604763979:-atm(C,c,22,B1),hidden_1_5(B,C,B1).
hidden_2(D,E):0.01096126540369869:-bond(E,C1,7),hidden_2_1(D,E,C1).
hidden_2(D,E):0.010398071805862172:-bond(D1,D,7),hidden_2_2(E,D1,D).
hidden_2(D,E):0.010586727137850348:-bond(D,E1,1).
hidden_3(F):0.011075927696865728:-ring_size_6(F).
hidden_4(G):0.011126539857872843:-ring_size_6(G).
hidden_5(H):0.011208098786886781:-ring_size_6(H).
hidden_6(K,L):0.010378304852704771:-bond(F1,K,7),hidden_6_1(L,F1,K).
hidden_6(K,L):0.010224487663076319:-bond(K,G1,1),hidden_6_2(L,K,G1).
hidden_6(K,L):0.00999581991554713:-bond(L,H1,1),hidden_6_3(K,L,H1).
hidden_6(K,L):0.010429377039920359:-atm(K,c,22,I1).
hidden_6(K,L):0.01019843325997902:-atm(L,c,22,I1),hidden_6_4(K,L,I1).
hidden_7(M):0.011149544666720199:-ring_size_6(M).
hidden_8(N):0.011256512833617312:-ring_size_6(N).
hidden_9(O):0.011244147922774698:-ring_size_6(O).
hidden_10(P):0.011403583497663851:-ring_size_6(P).
hidden_11(Q):0.011345431242200105:-ring_size_6(Q).
hidden_12(U,V):0.010445464524267683:-bond(V,J1,7),hidden_12_1(U,V,J1).
hidden_12(U,V):0.011187042502663374:-bond(K1,U,7).
hidden_12(U,V):0.010219926987081216:-bond(U,L1,1),hidden_12_3(V,U,L1).
hidden_12(U,V):0.010381262097911148:-atm(U,c,22,M1).
hidden_12(U,V):0.010129161461474351:-atm(V,c,22,M1),hidden_12_5(U,V,M1).
hidden_13(W):0.011179283545732478:-ring_size_6(W).
hidden_1_1(B,C,X):0.012441506010454262:-bond(X,N1,7).
hidden_1_1(B,C,X):0.012429246257106284:-bond(X,O1,7),hidden_1_1_2(B,C,X,O1).
hidden_1_2(C,Y,B):0.012487808314090867:-bond(Y,P1,1).
hidden_1_4(B,C,A1):0.012483417720555166:-atm(A1,h,3,Q1),hidden_1_4_1(B,C,A1,Q1).
hidden_1_5(B,C,B1):0.012491280872055881:-atm(Y,c,22,B1).
hidden_2_1(D,E,C1):0.012417892004433747:-bond(C1,R1,7),hidden_2_1_1(D,E,C1,R1).
hidden_2_2(E,D1,D):0.012469319469626906:-bond(D1,S1,1).
hidden_6_1(L,F1,K):0.01244958803023012:-bond(T1,F1,7),hidden_6_1_1(L,K,T1,F1).
hidden_6_2(L,K,G1):0.01248806082480572:-atm(G1,h,3,U1).
hidden_6_3(K,L,H1):0.012484015608182145:-atm(H1,h,3,U1),hidden_6_3_1(K,L,H1,U1).
hidden_6_4(K,L,I1):0.012491987092252304:-atm(V1,c,22,I1).
hidden_12_1(U,V,J1):0.01244430130492063:-bond(J1,W1,7),hidden_12_1_1(U,V,J1,W1).
hidden_12_1(U,V,J1):0.012471679072048283:-bond(J1,X1,1),hidden_12_1_2(U,V,J1,X1).
hidden_12_3(V,U,L1):0.012485426048129548:-atm(L1,h,3,Y1).
hidden_12_5(U,V,M1):0.01249208625042469:-atm(Z1,c,22,M1).
hidden_1_1_2(B,C,X,O1):0.01277711202041569:-bond(O1,A2,7),hidden_1_1_2_1(B,C,X,O1,A2).
hidden_1_1_2(B,C,X,O1):0.012778118153245087:-bond(O1,B2,1),hidden_1_1_2_2(B,C,X,O1,B2).
hidden_1_4_1(B,C,A1,Q1):0.01283270266245928:-atm(P1,h,3,Q1).
hidden_2_1_1(D,E,C1,R1):0.012771640811543652:-bond(R1,C2,7).
hidden_6_1_1(L,K,T1,F1):0.012796247567331123:-bond(T1,D2,1),hidden_6_1_1_1(L,K,F1,T1,D2).
hidden_6_3_1(K,L,H1,U1):0.012830419358789471:-atm(E2,h,3,U1).
hidden_12_1_1(U,V,J1,W1):0.012796496307035153:-bond(W1,F2,1).
hidden_12_1_2(U,V,J1,X1):0.01280198033618344:-bond(X1,G2,1),hidden_12_1_2_1(U,V,J1,X1,G2).
hidden_12_1_2(U,V,J1,X1):0.012809604724630885:-bond(X1,H2,1),hidden_12_1_2_2(U,V,J1,X1,H2).
hidden_12_1_2(U,V,J1,X1):0.012814984290722711:-bond(X1,I2,1).
hidden_12_1_2(U,V,J1,X1):0.012801839016031965:-atm(X1,c,10,J2),hidden_12_1_2_4(U,V,J1,X1,J2).
hidden_1_1_2_1(B,C,X,O1,A2):0.01307727570112182:-bond(A2,K2,7).
hidden_1_1_2_2(B,C,X,O1,B2):0.013077818799633377:-atm(B2,h,3,L2).
hidden_6_1_1_1(L,K,F1,T1,D2):0.013092696476377775:-atm(D2,h,3,M2).
hidden_12_1_2_1(U,V,J1,X1,G2):0.013093886493690605:-bond(F2,G2,2),hidden_12_1_2_1_1(U,V,J1,X1,F2,G2).
hidden_12_1_2_2(U,V,J1,X1,H2):0.013104509344678892:-atm(H2,h,3,N2).
hidden_12_1_2_4(U,V,J1,X1,J2):0.013097897150493507:-atm(O2,h,3,J2).
hidden_12_1_2_1_1(U,V,J1,X1,F2,G2):0.013355701673420666:-bond(F2,O2,1).

 Hyperparameters of muta_deep:

Save statistics? no
Number of arithmetic circuits: 169 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Parameters are initialized with values in the range [-0,50 0,50]
Eta= 0,001000 
Beta1= 0,900000 
Beta2= 0,999000 
Adam_hat= 0,0000000100  
BatchSize= 0 
Strategy= batch 
Regularization enabled
Regularization type:L2 
Gamma: 10,000000
