useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 12.755/19.00 = 0.671290
testsetLikelihood(sum(12.755), countOfExamples(19.00), mean(0.671290)).

// The weighted count of positive examples = 9.000 and the weighted count of negative examples = 10.000
weightedSumPos(9.000).
weightedSumNeg(10.000).

//  AUC ROC   = 0.883333
//  AUC PR    = 0.920988
//  CLL       = -0.483557
aucROC(active, 0.883333).
aucPR( active, 0.920988).

//   Precision = 0.875000 at threshold = 0.417
//   Recall    = 0.777778
//   F1        = 0.823529
precision(active, 0.875000, usingThreshold(0.4173289082865774)).
recall(   active, 0.777778, usingThreshold(0.4173289082865774)).
F1(       active, 0.823529, usingThreshold(0.4173289082865774)).

