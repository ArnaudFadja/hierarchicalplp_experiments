useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 10.802/19.00 = 0.568507
testsetLikelihood(sum(10.802), countOfExamples(19.00), mean(0.568507)).

// The weighted count of positive examples = 13.000 and the weighted count of negative examples = 6.000
weightedSumPos(13.000).
weightedSumNeg(6.000).

//  AUC ROC   = 0.948718
//  AUC PR    = 0.978932
//  CLL       = -0.627595
aucROC(active, 0.948718).
aucPR( active, 0.978932).

//   Precision = 1.000000 at threshold = 0.339
//   Recall    = 0.846154
//   F1        = 0.916667
precision(active, 1.000000, usingThreshold(0.3385476967494796)).
recall(   active, 0.846154, usingThreshold(0.3385476967494796)).
F1(       active, 0.916667, usingThreshold(0.3385476967494796)).

