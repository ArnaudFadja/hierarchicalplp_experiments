useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 11.619/19.00 = 0.611520
testsetLikelihood(sum(11.619), countOfExamples(19.00), mean(0.611520)).

// The weighted count of positive examples = 14.000 and the weighted count of negative examples = 5.000
weightedSumPos(14.000).
weightedSumNeg(5.000).

//  AUC ROC   = 0.957143
//  AUC PR    = 0.985476
//  CLL       = -0.568239
aucROC(active, 0.957143).
aucPR( active, 0.985476).

//   Precision = 0.923077 at threshold = 0.364
//   Recall    = 0.857143
//   F1        = 0.888889
precision(active, 0.923077, usingThreshold(0.3643247735662109)).
recall(   active, 0.857143, usingThreshold(0.3643247735662109)).
F1(       active, 0.888889, usingThreshold(0.3643247735662109)).

