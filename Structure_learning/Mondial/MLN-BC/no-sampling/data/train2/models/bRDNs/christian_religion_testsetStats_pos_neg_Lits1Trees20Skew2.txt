useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 16.986/44.00 = 0.386044
testsetLikelihood(sum(16.986), countOfExamples(44.00), mean(0.386044)).

// The weighted count of positive examples = 29.000 and the weighted count of negative examples = 15.000
weightedSumPos(29.000).
weightedSumNeg(15.000).

//  AUC ROC   = 0.339080
//  AUC PR    = 0.565493
//  CLL       = -1.296930
aucROC(christian_religion, 0.339080).
aucPR( christian_religion, 0.565493).

//   Precision = 0.500000 at threshold = 0.160
//   Recall    = 0.344828
//   F1        = 0.408163
precision(christian_religion, 0.500000, usingThreshold(0.16022642694323674)).
recall(   christian_religion, 0.344828, usingThreshold(0.16022642694323674)).
F1(       christian_religion, 0.408163, usingThreshold(0.16022642694323674)).

