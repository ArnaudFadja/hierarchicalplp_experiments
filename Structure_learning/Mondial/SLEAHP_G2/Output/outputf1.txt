Hyper-parameters for generating the hierachical PLP 

The initial HPLP is generated using the entire bottom clauses
Initial Probability=1.000000 
Rate=1.000000 
MaxLayer=2 
Time=12.045000 
LL: -35.68977573893414
AUCROC: 0.3738095238095238
AUCPR: 0.6048399968168727
Learned Program
christian_religion(A):0.02383879640612865:-politics(A,B,C,D,E).
christian_religion(A):0.02379829776454624:-economy(A,F,G,H,I,J).
christian_religion(A):0.023824750870113723:-ethnicGroup(A,K,L).
christian_religion(A):0.02378773560443674:-borders(A,M,N),hidden_1(A,M,N).
christian_religion(A):0.02380431425173275:-encompasses(A,O,P),hidden_2(A,O,P).
christian_religion(A):0.023840045508975716:-isMember(A,Q,R).
christian_religion(A):0.023798833749813196:-encompasses(A,S,T),hidden_3(A,S,T).
christian_religion(A):0.023860432569315346:-language(A,U,V),hidden_4(A,U,V).
christian_religion(A):0.02379734787807784:-encompasses(A,W,X),hidden_5(A,W,X).
christian_religion(A):0.023819626568447525:-borders(Y,A,Z),hidden_6(Y,A,Z).
christian_religion(A):0.0238428262420508:-encompasses(A,A1,B1),hidden_7(A,A1,B1).
christian_religion(A):0.023811643049517446:-language(A,C1,D1),hidden_8(A,C1,D1).
christian_religion(A):0.023870315015752427:-borders(A,E1,F1).
christian_religion(A):0.02383383413713342:-language(A,G1,H1),hidden_9(A,G1,H1).
christian_religion(A):0.023789373387812836:-encompasses(A,I1,J1),hidden_10(A,I1,J1).
christian_religion(A):0.023822965617085627:-encompasses(A,K1,L1),hidden_11(A,K1,L1).
hidden_1(A,M,N):0.02379998199158356:-borders(M1,M,N1).
hidden_2(A,O,P):0.023792375513572286:-encompasses(O1,O,P).
hidden_3(A,S,T):0.02382881096347371:-encompasses(P1,S,T).
hidden_4(A,U,V):0.023804226018950502:-language(Q1,U,V).
hidden_5(A,W,X):0.023797537318059038:-encompasses(R1,W,X).
hidden_6(Y,A,Z):0.023794668115316074:-isMember(Y,S1,T1).
hidden_7(A,A1,B1):0.02379888575014778:-encompasses(U1,A1,B1).
hidden_8(A,C1,D1):0.023863347325818754:-language(E1,C1,V1).
hidden_9(A,G1,H1):0.023843663290590282:-language(W1,G1,X1).
hidden_10(A,I1,J1):0.023814707356950463:-encompasses(Y1,I1,J1).
hidden_11(A,K1,L1):0.023841172102761893:-encompasses(Z1,K1,L1).

 Hyperparameters of mondial_deep:

Save statistics? no
Number of arithmetic circuits: 176 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Parameters are initialized with values in the range [-0,50 0,50]
Eta= 0,001000 
Beta1= 0,900000 
Beta2= 0,999000 
Adam_hat= 0,0000000100  
BatchSize= 0 
Strategy= batch 
Regularization enabled
Regularization type:L2 
Gamma: 10,000000
