Hyper-parameters for generating the hierachical PLP 

The initial HPLP is generated using the entire bottom clauses
Initial Probability=1.000000 
Rate=1.000000 
MaxLayer=2 
Time=13.390000 
LL: -33.42985918169097
AUCROC: 0.5057471264367817
AUCPR: 0.7251578406573168
Learned Program
christian_religion(A):0.023335807316112005:-politics(A,B,C,D,E).
christian_religion(A):0.02329778375027477:-economy(A,F,G,H,I,J).
christian_religion(A):0.02332262111364067:-ethnicGroup(A,K,L).
christian_religion(A):0.02328786564050532:-borders(A,M,N),hidden_1(A,M,N).
christian_religion(A):0.023303433011543646:-encompasses(A,O,P),hidden_2(A,O,P).
christian_religion(A):0.02333697995148759:-isMember(A,Q,R).
christian_religion(A):0.023298287008522284:-encompasses(A,S,T),hidden_3(A,S,T).
christian_religion(A):0.023356118183706774:-language(A,U,V),hidden_4(A,U,V).
christian_religion(A):0.023296891801421058:-encompasses(A,W,X),hidden_5(A,W,X).
christian_religion(A):0.023317810138185013:-borders(Y,A,Z),hidden_6(Y,A,Z).
christian_religion(A):0.023339590420563962:-encompasses(A,A1,B1),hidden_7(A,A1,B1).
christian_religion(A):0.023310314494468888:-language(A,C1,D1),hidden_8(A,C1,D1).
christian_religion(A):0.023365393853686366:-borders(A,E1,F1).
christian_religion(A):0.02333114893148644:-language(A,G1,H1),hidden_9(A,G1,H1).
christian_religion(A):0.023289403717887396:-encompasses(A,I1,J1),hidden_10(A,I1,J1).
christian_religion(A):0.023320944599328333:-borders(A,K1,L1),hidden_11(A,K1,L1).
hidden_1(A,M,N):0.02329936486769629:-borders(M1,M,N1).
hidden_2(A,O,P):0.023292222663697616:-encompasses(O1,O,P).
hidden_3(A,S,T):0.023326432731649274:-encompasses(P1,S,T).
hidden_4(A,U,V):0.023303350375072175:-language(Q1,U,V).
hidden_5(A,W,X):0.023297069585249977:-encompasses(R1,W,X).
hidden_6(Y,A,Z):0.023294375630686592:-isMember(Y,S1,T1).
hidden_7(A,A1,B1):0.023298335711902426:-encompasses(U1,A1,B1).
hidden_8(A,C1,D1):0.023358854201433164:-language(E1,C1,V1).
hidden_9(A,G1,H1):0.023340376428082385:-language(W1,G1,X1).
hidden_10(A,I1,J1):0.023313191290567633:-encompasses(Y1,I1,J1).
hidden_11(A,K1,L1):0.023338037077525655:-encompasses(K1,Z1,A2).
hidden_11(A,K1,L1):0.023308534859273474:-isMember(K1,B2,C2).

 Hyperparameters of mondial_deep:

Save statistics? no
Number of arithmetic circuits: 176 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Parameters are initialized with values in the range [-0,50 0,50]
Eta= 0,001000 
Beta1= 0,900000 
Beta2= 0,999000 
Adam_hat= 0,0000000100  
BatchSize= 0 
Strategy= batch 
Regularization enabled
Regularization type:L2 
Gamma: 10,000000
