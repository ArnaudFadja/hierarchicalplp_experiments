Hyper-parameters for generating the hierachical PLP 

The initial HPLP is generated using the entire bottom clauses
Initial Probability=1.000000 
Rate=1.000000 
MaxLayer=2 
Time=47.137000 
LL: -135.8010380519273
AUCROC: 0.3229813664596273
AUCPR: 0.5287027640071115
Learned Program
christian_religion(A):0.00010266262735588389:-politics(A,B,C,D,E).
christian_religion(A):0.00010252667416876:-economy(A,F,G,H,I,J).
christian_religion(A):0.00010233503268667474:-ethnicGroup(A,K,L).
christian_religion(A):0.00010227766308129955:-borders(A,M,N),hidden_1(A,M,N).
christian_religion(A):0.00010254880783616453:-encompasses(A,O,P),hidden_2(A,O,P).
christian_religion(A):0.00010194044888123144:-isMember(A,Q,R).
christian_religion(A):0.00010253349803193559:-encompasses(A,S,T),hidden_3(A,S,T).
christian_religion(A):0.00010088971379724777:-borders(U,A,V),hidden_4(U,A,V).
christian_religion(A):0.00010252964884873076:-encompasses(A,W,X),hidden_5(A,W,X).
christian_religion(A):0.00010276110180148644:-language(A,Y,Z),hidden_6(A,Y,Z).
christian_religion(A):0.00010053790804749678:-language(A,A1,B1).
christian_religion(A):0.00010066078298213914:-borders(C1,A,D1),hidden_7(C1,A,D1).
christian_religion(A):0.00010281723619985833:-encompasses(A,E1,F1),hidden_8(A,E1,F1).
christian_religion(A):0.00010241058869753173:-borders(A,G1,H1),hidden_9(A,G1,H1).
christian_religion(A):0.00010062254347479464:-borders(I1,A,J1).
christian_religion(A):0.00010260955318625018:-encompasses(A,K1,L1),hidden_10(A,K1,L1).
hidden_1(A,M,N):0.00019560804843831594:-borders(M1,M,N1).
hidden_2(A,O,P):0.0001959624492203874:-encompasses(O1,O,P).
hidden_3(A,S,T):0.00019600404053947009:-encompasses(P1,S,T).
hidden_4(U,A,V):0.000195527830387051:-isMember(U,Q1,R1).
hidden_5(A,W,X):0.00019585424519900227:-encompasses(S1,W,X).
hidden_6(A,Y,Z):0.00019624000942118023:-language(T1,Y,U1).
hidden_7(C1,A,D1):0.00019236500480580165:-encompasses(C1,V1,W1).
hidden_8(A,E1,F1):0.00019699193546823048:-encompasses(X1,E1,F1).
hidden_9(A,G1,H1):0.00019659272038796143:-borders(Y1,G1,Z1).
hidden_10(A,K1,L1):0.00019634971016736368:-encompasses(A2,K1,L1).

 Hyperparameters of mondial_deep:

Save statistics? no
Number of arithmetic circuits: 176 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Parameters are initialized with values in the range [-0,50 0,50]
Eta= 0,001000 
Beta1= 0,900000 
Beta2= 0,999000 
Adam_hat= 0,0000000100  
BatchSize= 0 
Strategy= batch 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
