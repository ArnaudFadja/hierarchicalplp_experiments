Hyper-parameters for generating the hierachical PLP 

The initial HPLP is generated using the entire bottom clauses
Initial Probability=1.000000 
Rate=1.000000 
MaxLayer=2 
Time=48.485000 
LL: -76.43520674752017
AUCROC: 0.34782608695652173
AUCPR: 0.4593825937342128
Learned Program
christian_religion(A):0.24322097061247705:-politics(A,B,C,D,E).
christian_religion(A):0.2402035271689004:-economy(A,F,G,H,I,J).
christian_religion(A):0.16339517717238036:-encompasses(A,K,L),hidden_2(A,K,L).
christian_religion(A):0.005732250620147594:-isMember(A,M,N).
christian_religion(A):0.0002674740158860045:-encompasses(A,O,P),hidden_3(A,O,P).
christian_religion(A):0.6968257198542911:-borders(Q,A,R).
christian_religion(A):0.04672313405838474:-encompasses(A,S,T),hidden_5(A,S,T).
christian_religion(A):0.1898252532976645:-language(A,U,V),hidden_6(A,U,V).
christian_religion(A):0.023653241089369947:-borders(W,A,X).
christian_religion(A):0.4465097206126542:-encompasses(A,Y,Z),hidden_8(A,Y,Z).
christian_religion(A):0.1502513462643516:-encompasses(A,A1,B1),hidden_10(A,A1,B1).
hidden_2(A,K,L):0.004408715134552932:-encompasses(C1,K,L).
hidden_3(A,O,P):0.3532156790463527:-encompasses(D1,O,P).
hidden_5(A,S,T):0.049514070799842935:-encompasses(E1,S,T).
hidden_6(A,U,V):0.18757384867515503:-language(F1,U,G1).
hidden_8(A,Y,Z):0.0001960185739637299:-encompasses(H1,Y,Z).
hidden_10(A,A1,B1):0.016985055753514:-encompasses(I1,A1,B1).

 Hyperparameters of mondial_deep:

Save statistics? no
Number of arithmetic circuits: 176 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Regularization enabled
Regularization type:L2 
Gamma: 10,000000
