useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 24.822/44.00 = 0.564126
testsetLikelihood(sum(24.822), countOfExamples(44.00), mean(0.564126)).

// The weighted count of positive examples = 23.000 and the weighted count of negative examples = 21.000
weightedSumPos(23.000).
weightedSumNeg(21.000).

//  AUC ROC   = 0.664596
//  AUC PR    = 0.623041
//  CLL       = -0.720645
aucROC(christian_religion, 0.664596).
aucPR( christian_religion, 0.623041).

//   Precision = 0.666667 at threshold = 0.821
//   Recall    = 0.260870
//   F1        = 0.375000
precision(christian_religion, 0.666667, usingThreshold(0.8207737449608247)).
recall(   christian_religion, 0.260870, usingThreshold(0.8207737449608247)).
F1(       christian_religion, 0.375000, usingThreshold(0.8207737449608247)).

