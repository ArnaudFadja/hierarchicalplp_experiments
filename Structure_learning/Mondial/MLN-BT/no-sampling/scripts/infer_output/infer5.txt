args[0] = -aucJarPath
args[2] = -target
args[4] = -trees
args[6] = -model
args[8] = -testNegPosRatio
args[10] = -save
args[11] = -i
args[12] = -test
cmd.getTestDirVal()../data/test5/
test5

% Starting an INFERENCE run of bRDN.
% Running on host: r240n01

% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.

Resetting the LazyGroundNthArgumentClauseIndex.

% Calling ILPouterLoop from createRegressionOuterLooper.

% getInputArgWithDefaultValue: args=[../data/test5/test5_pos.txt, ../data/test5/test5_neg.txt, ../data/test5/test5_bk.txt, ../data/test5/test5_facts.txt]
%  for N=0: args[N]=../data/test5/test5_pos.txt

% getInputArgWithDefaultValue: args=[../data/test5/test5_pos.txt, ../data/test5/test5_neg.txt, ../data/test5/test5_bk.txt, ../data/test5/test5_facts.txt]
%  for N=1: args[N]=../data/test5/test5_neg.txt

% getInputArgWithDefaultValue: args=[../data/test5/test5_pos.txt, ../data/test5/test5_neg.txt, ../data/test5/test5_bk.txt, ../data/test5/test5_facts.txt]
%  for N=2: args[N]=../data/test5/test5_bk.txt

% getInputArgWithDefaultValue: args=[../data/test5/test5_pos.txt, ../data/test5/test5_neg.txt, ../data/test5/test5_bk.txt, ../data/test5/test5_facts.txt]
%  for N=3: args[N]=../data/test5/test5_facts.txt

% Welcome to the WILL ILP/SRL systems.


% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.
% Reading background theory from dir: null
% Load '../mondial_bk'.

% Switching to VarIndicator = uppercase.

% Switching to standard-logic notation for variables; previous setting = uppercase

% Switching to VarIndicator = lowercase.

***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****


***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****

% [ LazyGroundClauseIndex ]  Building full index for mode/1 with 1 assertions.
% LoadAllModes() called.  Currently loaded modes: []

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.
% [ LazyGroundClauseIndex ]  Building full index for sameAs/2 with 2 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for exp/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for log/3.
% LoadAllLibraries() called.  Currently loaded libraries: [listsInLogic, differentInLogic, modes_arithmeticInLogic, inlines_comparisonInLogic, modes_listsInLogic, inlines_differentInLogic, modes_differentInLogic, arithmeticInLogic, inlines_listsInLogic, modes_comparisonInLogic, comparisonInLogic, inlines_arithmeticInLogic]

%  Read the facts.
%  Have read 10,413 facts.
% Have read 30 examples from '../data/test5' [../data/test5/test5*].
% Have read 14 examples from '../data/test5' [../data/test5/test5*].

%  LearnOneClause initialized.

% The outer looper has been created.

% Initializing the ILP inner looper.

% NEW target:                 christian_religion(a)
%  targetPred:                christian_religion/1
%  targetArgTypes:            signature = [Const], types = [+State]
%  targets:                   [christian_religion(a)]
%  targetPredicates:          [christian_religion/1]
%  targetArgSpecs:            [[a[+State]]]
%  variablesInTargets:        [[a]]

% Started collecting constants

% Collecting the types of constants.

%   *** WARNING ***  Constant '"Albania"' is already marked as being of types = [Country];
%          type = 'Province' may be added if not already known.
%  PredicateName = 'country', from 'country("Albania", "AL", "Tirane", "Albania", 28750, 3249136)',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [-Country, +State, -Capital, -Province, -AreaS, -PopN]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"Luxembourg"' is already marked as being of types = [Country];
%          type = 'Capital' may be added if not already known.
%  PredicateName = 'country', from 'country("Luxembourg", "L", "Luxembourg", "Luxembourg", 2586, 415870)',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [-Country, +State, -Capital, -Province, -AreaS, -PopN]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province];
%          type = 'Pop' may be added if not already known.
%  PredicateName = 'population', from 'population("MNE", -0.851, "null")',
%  which has types = [signature = [Const, Const, Const], types = [+State, -Popgr, -Pop]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province, Pop];
%          type = 'Popgr' may be added if not already known.
%  PredicateName = 'population', from 'population("KOS", "null", "null")',
%  which has types = [signature = [Const, Const, Const], types = [+State, -Popgr, -Pop]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province, Pop, Popgr];
%          type = 'Motherstate' may be added if not already known.
%  PredicateName = 'politics', from 'politics("AL", "1912-11-28", "Ottoman Empire", "null", "emerging democracy")',
%  which has types = [signature = [Const, Const, Const, Const, Const], types = [+State, -Date, -Name, -Motherstate, -Type]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"SRB"' is already marked as being of types = [State];
%          type = 'Name' may be added if not already known.
%  PredicateName = 'politics', from 'politics("MNE", "2006-06-03", "SRB", "null", "parliamentary democracy")',
%  which has types = [signature = [Const, Const, Const, Const, Const], types = [+State, -Date, -Name, -Motherstate, -Type]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province, Pop, Popgr, Motherstate];
%          type = 'Date' may be added if not already known.
%  PredicateName = 'politics', from 'politics("AND", "null", "null", "null", "parliamentary democracy that retains as its heads of state a coprincipality")',
%  which has types = [signature = [Const, Const, Const, Const, Const], types = [+State, -Date, -Name, -Motherstate, -Type]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province, Pop, Popgr, Motherstate, Date, Name];
%          type = 'Type' may be added if not already known.
%  PredicateName = 'politics', from 'politics("GAZA", "null", "null", "null", "null")',
%  which has types = [signature = [Const, Const, Const, Const, Const], types = [+State, -Date, -Name, -Motherstate, -Type]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province, Pop, Popgr, Motherstate, Date, Name, Type];
%          type = 'Serv' may be added if not already known.
%  PredicateName = 'economy', from 'economy("AL", 4100, 55, "null", "null", 16)',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+State, -Gdp, -Agric, -Serv, -Indus, -Infl]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province, Pop, Popgr, Motherstate, Date, Name, Type, Serv];
%          type = 'Indus' may be added if not already known.
%  PredicateName = 'economy', from 'economy("AL", 4100, 55, "null", "null", 16)',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+State, -Gdp, -Agric, -Serv, -Indus, -Infl]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '16' is already marked as being of types = [AreaS];
%          type = 'Infl' may be added if not already known.
%  PredicateName = 'economy', from 'economy("AL", 4100, 55, "null", "null", 16)',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+State, -Gdp, -Agric, -Serv, -Indus, -Infl]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '24' is already marked as being of types = [Pop];
%          type = 'Agric' may be added if not already known.
%  PredicateName = 'economy', from 'economy("MK", 1900, 24, 44, 32, 14.8)',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+State, -Gdp, -Agric, -Serv, -Indus, -Infl]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province, Pop, Popgr, Motherstate, Date, Name, Type, Serv, Indus, Agric, Infl];
%          type = 'Gdp' may be added if not already known.
%  PredicateName = 'economy', from 'economy("GBG", "null", "null", "null", "null", 7)',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+State, -Gdp, -Agric, -Serv, -Indus, -Infl]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '21' is already marked as being of types = [AreaS, Agric, Serv];
%          type = 'Perl' may be added if not already known.
%  PredicateName = 'language', from 'language("MK", "Albanian", 21)',
%  which has types = [signature = [Const, Const, Const], types = [+State, -Language, -Perl], signature = [Const, Const, Const], types = [-State, +Language, -Perl]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '3' is already marked as being of types = [Agric, Infl, Perl];
%          type = 'Perg' may be added if not already known.
%  PredicateName = 'ethnicGroup', from 'ethnicGroup("AL", "Greeks", 3)',
%  which has types = [signature = [Const, Const, Const], types = [+State, -Group, -Perg]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"Albanian"' is already marked as being of types = [Language];
%          type = 'Group' may be added if not already known.
%  PredicateName = 'ethnicGroup', from 'ethnicGroup("AL", "Albanian", 95)',
%  which has types = [signature = [Const, Const, Const], types = [+State, -Group, -Perg]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '100' is already marked as being of types = [Gdp, Perl, Perg];
%          type = 'Pere' may be added if not already known.
%  PredicateName = 'encompasses', from 'encompasses("AL", "Europe", 100)',
%  which has types = [signature = [Const, Const, Const], types = [+State, -Cont, -Pere], signature = [Const, Const, Const], types = [-State, +Cont, -Pere]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '228' is already marked as being of types = [Gdp];
%          type = 'Length' may be added if not already known.
%  PredicateName = 'borders', from 'borders("GR", "MK", 228)',
%  which has types = [signature = [Const, Const, Const], types = [+State, -State, -Length]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"Brussels"' is already marked as being of types = [Capital];
%          type = 'City' may be added if not already known.
%  PredicateName = 'organization', from 'organization("ACP", "African, Caribbean, and Pacific Countries", "Brussels", "B", "Brabant", "1976-04-01")',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+Org, -Name, -City, -State, -Region, -Date], signature = [Const, Const, Const, Const, Const, Const], types = [-Org, -Name, -City, +State, -Region, -Date]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"Brabant"' is already marked as being of types = [Province];
%          type = 'Region' may be added if not already known.
%  PredicateName = 'organization', from 'organization("ACP", "African, Caribbean, and Pacific Countries", "Brussels", "B", "Brabant", "1976-04-01")',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+Org, -Name, -City, -State, -Region, -Date], signature = [Const, Const, Const, Const, Const, Const], types = [-Org, -Name, -City, +State, -Region, -Date]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"AG"' is already marked as being of types = [State];
%          type = 'Org' may be added if not already known.
%  PredicateName = 'organization', from 'organization("AG", "Andean Group", "Lima", "PE", "Lima", "1969-05-26")',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+Org, -Name, -City, -State, -Region, -Date], signature = [Const, Const, Const, Const, Const, Const], types = [-Org, -Name, -City, +State, -Region, -Date]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

%   *** WARNING ***  Constant '"null"' is already marked as being of types = [Capital, Province, Pop, Popgr, Motherstate, Date, Name, Type, Serv, Indus, Agric, Infl, Gdp, City];
%          type = 'State' may be added if not already known.
%  PredicateName = 'organization', from 'organization("ACC", "Arab Cooperation Council", "null", "null", "null", "1989-02-16")',
%  which has types = [signature = [Const, Const, Const, Const, Const, Const], types = [+Org, -Name, -City, -State, -Region, -Date], signature = [Const, Const, Const, Const, Const, Const], types = [-Org, -Name, -City, +State, -Region, -Date]]
%   Other warnings with this predicate and this new type are not reported in order to keep this printout small.  Use dontComplainAboutMultipleTypes to override.

% Looking at the training examples to see if any types of new constants can be inferred.
% Time to collect constants: 810 milliseconds
% Time to collect examples: 0 seconds

% Read 30 pos examples and 14 neg examples.
% Time to init learnOneClause: 935 milliseconds
% Old dir../data/train5/models/

% Have 30 'raw' positive examples and kept 30.
% Have 14 'raw' negative examples and kept 14.

% processing backup's for christian_religion
%  POS EX = 30
%  NEG EX = 14

% Memory usage by WILLSetup (just counts # targets?):
%  |backupPosExamples| = 1
%  |backupNegExamples| = 1
%  |predicatesAsFacts| = 0
%  |addedToFactBase|   = 0

% Getting bRDN's target predicates.
% Did not learn a model for 'christian_religion' this run.
%   loadModel (#0): ../data/train5/models/bRDNs/Trees/christian_religionTree0.tree
%   loadModel (#1): ../data/train5/models/bRDNs/Trees/christian_religionTree1.tree
%   loadModel (#2): ../data/train5/models/bRDNs/Trees/christian_religionTree2.tree
%   loadModel (#3): ../data/train5/models/bRDNs/Trees/christian_religionTree3.tree
%   loadModel (#4): ../data/train5/models/bRDNs/Trees/christian_religionTree4.tree
%   loadModel (#5): ../data/train5/models/bRDNs/Trees/christian_religionTree5.tree
%   loadModel (#6): ../data/train5/models/bRDNs/Trees/christian_religionTree6.tree
%   loadModel (#7): ../data/train5/models/bRDNs/Trees/christian_religionTree7.tree
%   loadModel (#8): ../data/train5/models/bRDNs/Trees/christian_religionTree8.tree
%   loadModel (#9): ../data/train5/models/bRDNs/Trees/christian_religionTree9.tree
%   loadModel (#10): ../data/train5/models/bRDNs/Trees/christian_religionTree10.tree
%   loadModel (#11): ../data/train5/models/bRDNs/Trees/christian_religionTree11.tree
%   loadModel (#12): ../data/train5/models/bRDNs/Trees/christian_religionTree12.tree
%   loadModel (#13): ../data/train5/models/bRDNs/Trees/christian_religionTree13.tree
%   loadModel (#14): ../data/train5/models/bRDNs/Trees/christian_religionTree14.tree
%   loadModel (#15): ../data/train5/models/bRDNs/Trees/christian_religionTree15.tree
%   loadModel (#16): ../data/train5/models/bRDNs/Trees/christian_religionTree16.tree
%   loadModel (#17): ../data/train5/models/bRDNs/Trees/christian_religionTree17.tree
%   loadModel (#18): ../data/train5/models/bRDNs/Trees/christian_religionTree18.tree
%   loadModel (#19): ../data/train5/models/bRDNs/Trees/christian_religionTree19.tree
%  Done loading 20 models.
File: ../data/test5/advice.txt doesnt exist.Hence no advice loaded

% for christian_religion |lookupPos| = 30
% for christian_religion |lookupNeg| = 14
% getJointExamples: |pos| = 30, |neg| = 14

% Starting inference in bRDN.
% Trees = 20

% Starting getMarginalProbabilities.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for borders/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for ethnicGroup/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for language/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for ethnicGroup/3.
% [ LazyGroundClauseIndex ]  Building full index for ethnicGroup/3 with 551 assertions.
% [ LazyGroundClauseIndex ]  Building full index for equalGroup/2 with 1 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for language/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for borders/3.
% No Gibbs sampling needed during inference.
Best F1 = 1.0
 (Arithmetic) Mean Probability Assigned to Correct Output Class: 20.031/44.00 = 0.455251

 The weighted count of positive examples = 30.000 and the weighted count of negative examples = 14.000

printExamples: Writing out predictions (for Tuffy?) on 44 examples for 'christian_religion' to:
  ../data/test5/results_christian_religion.db
 and to:
  ../data/test5/query_christian_religion.db
%    No need to compress since small: ../data/test5/query_christian_religion.db

% Computing Area Under Curves.
%Pos=30
%Neg=14
%LL:-32.610424454609536
%LL:-41.87918902543369

% Running command: java -jar ../../../../Boostr/auc.jar ../data/test5/AUC/aucTemp.txt list 0.0
% WAITING FOR command: java -jar ../../../../Boostr/auc.jar ../data/test5/AUC/aucTemp.txt list 0.0
% DONE WAITING FOR command: java -jar ../../../../Boostr/auc.jar ../data/test5/AUC/aucTemp.txt list 0.0

%   AUC ROC   = 0.533333
%   AUC PR    = 0.690001
%   CLL	      = -0.951800
%   Precision = 0.695652 at threshold = 0.417
%   Recall    = 0.533333
%   F1        = 0.603774

% Total inference time (20 trees): 6.381 seconds.
