args[0] = -aucJarPath
args[2] = -target
args[4] = -trees
args[6] = -model
args[8] = -testNegPosRatio
args[10] = -save
args[11] = -i
args[12] = -test
cmd.getTestDirVal()../data/test3/
test3

% Starting an INFERENCE run of bRDN.
% Running on host: r240n01

% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.

Resetting the LazyGroundNthArgumentClauseIndex.

% Calling ILPouterLoop from createRegressionOuterLooper.

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=0: args[N]=../data/test3/test3_pos.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=1: args[N]=../data/test3/test3_neg.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=2: args[N]=../data/test3/test3_bk.txt

% getInputArgWithDefaultValue: args=[../data/test3/test3_pos.txt, ../data/test3/test3_neg.txt, ../data/test3/test3_bk.txt, ../data/test3/test3_facts.txt]
%  for N=3: args[N]=../data/test3/test3_facts.txt

% Welcome to the WILL ILP/SRL systems.


% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.
% Reading background theory from dir: null
% Load '../uwcse_bk.txt'.

% Switching to VarIndicator = uppercase.

% Switching to standard-logic notation for variables; previous setting = uppercase

% Switching to VarIndicator = lowercase.

***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****


***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****

% [ LazyGroundClauseIndex ]  Building full index for mode/1 with 1 assertions.
% LoadAllModes() called.  Currently loaded modes: []

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.
% [ LazyGroundClauseIndex ]  Building full index for sameAs/2 with 2 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for exp/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for log/3.
% LoadAllLibraries() called.  Currently loaded libraries: [listsInLogic, differentInLogic, modes_arithmeticInLogic, inlines_comparisonInLogic, modes_listsInLogic, inlines_differentInLogic, modes_differentInLogic, arithmeticInLogic, inlines_listsInLogic, modes_comparisonInLogic, comparisonInLogic, inlines_arithmeticInLogic]

%  Read the facts.
%  Have read 191 facts.
% Have read 9 examples from '../data/test3' [../data/test3/test3*].
% Have read 775 examples from '../data/test3' [../data/test3/test3*].

%  LearnOneClause initialized.

% The outer looper has been created.

% Initializing the ILP inner looper.

% NEW target:                 advisedby(a, b)
%  targetPred:                advisedby/2
%  targetArgTypes:            signature = [Const, Const], types = [+Person, +Person]
%  targets:                   [advisedby(a, b)]
%  targetPredicates:          [advisedby/2]
%  targetArgSpecs:            [[a[+Person], b[+Person]]]
%  variablesInTargets:        [[a, b]]

% Started collecting constants

% Collecting the types of constants.

% Looking at the training examples to see if any types of new constants can be inferred.
% Time to collect constants: 71 milliseconds
% Time to collect examples: 0 seconds

% Read 9 pos examples and 775 neg examples.
% Time to init learnOneClause: 88 milliseconds
% Old dir../data/train3/models/

% Have 9 'raw' positive examples and kept 9.
% Have 775 'raw' negative examples and kept 775.

% processing backup's for advisedby
%  POS EX = 9
%  NEG EX = 775

% Memory usage by WILLSetup (just counts # targets?):
%  |backupPosExamples| = 1
%  |backupNegExamples| = 1
%  |predicatesAsFacts| = 0
%  |addedToFactBase|   = 0

% Getting bRDN's target predicates.
% Did not learn a model for 'advisedby' this run.
%   loadModel (#0): ../data/train3/models/bRDNs/Trees/advisedbyTree0.tree
%   loadModel (#1): ../data/train3/models/bRDNs/Trees/advisedbyTree1.tree
%   loadModel (#2): ../data/train3/models/bRDNs/Trees/advisedbyTree2.tree
%   loadModel (#3): ../data/train3/models/bRDNs/Trees/advisedbyTree3.tree
%   loadModel (#4): ../data/train3/models/bRDNs/Trees/advisedbyTree4.tree
%   loadModel (#5): ../data/train3/models/bRDNs/Trees/advisedbyTree5.tree
%   loadModel (#6): ../data/train3/models/bRDNs/Trees/advisedbyTree6.tree
%   loadModel (#7): ../data/train3/models/bRDNs/Trees/advisedbyTree7.tree
%   loadModel (#8): ../data/train3/models/bRDNs/Trees/advisedbyTree8.tree
%   loadModel (#9): ../data/train3/models/bRDNs/Trees/advisedbyTree9.tree
%   loadModel (#10): ../data/train3/models/bRDNs/Trees/advisedbyTree10.tree
%   loadModel (#11): ../data/train3/models/bRDNs/Trees/advisedbyTree11.tree
%   loadModel (#12): ../data/train3/models/bRDNs/Trees/advisedbyTree12.tree
%   loadModel (#13): ../data/train3/models/bRDNs/Trees/advisedbyTree13.tree
%   loadModel (#14): ../data/train3/models/bRDNs/Trees/advisedbyTree14.tree
%   loadModel (#15): ../data/train3/models/bRDNs/Trees/advisedbyTree15.tree
%   loadModel (#16): ../data/train3/models/bRDNs/Trees/advisedbyTree16.tree
%   loadModel (#17): ../data/train3/models/bRDNs/Trees/advisedbyTree17.tree
%   loadModel (#18): ../data/train3/models/bRDNs/Trees/advisedbyTree18.tree
%   loadModel (#19): ../data/train3/models/bRDNs/Trees/advisedbyTree19.tree
%  Done loading 20 models.
File: ../data/test3/advice.txt doesnt exist.Hence no advice loaded

% for advisedby |lookupPos| = 9
% for advisedby |lookupNeg| = 775
% getJointExamples: |pos| = 9, |neg| = 775

% Starting inference in bRDN.
% Trees = 20

% Starting getMarginalProbabilities.
% [ LazyGroundClauseIndex ]  Building full index for professor/1 with 8 assertions.
% [ LazyGroundClauseIndex ]  Building full index for student/1 with 20 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for publication/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for ta/3.
% [ LazyGroundClauseIndex ]  Building full index for taughtBy/3 with 29 assertions.
% [ LazyGroundClauseIndex ]  Building full index for inPhase/2 with 10 assertions.
% [ LazyGroundClauseIndex ]  Building full index for courseLevel/2 with 14 assertions.
% [ LazyGroundClauseIndex ]  Building full index for hasPosition/2 with 4 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for taughtBy/3.
% [ LazyGroundClauseIndex ]  Building full index for ta/3 with 26 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for ta/3.
% [ LazyGroundClauseIndex ]  Building full index for publication/2 with 10 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for publication/2.
% [ LazyGroundClauseIndex ]  Building full index for yearsInProgram/2 with 10 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for tempAdvisedBy/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for taughtBy/3.
% No Gibbs sampling needed during inference.
Best F1 = 1.0
 (Arithmetic) Mean Probability Assigned to Correct Output Class: 737.553/784.00 = 0.940756

 The weighted count of positive examples = 9.000 and the weighted count of negative examples = 775.000

printExamples: Writing out predictions (for Tuffy?) on 784 examples for 'advisedby' to:
  ../data/test3/results_advisedby.db
 and to:
  ../data/test3/query_advisedby.db
%    No need to compress since small: ../data/test3/query_advisedby.db

% Computing Area Under Curves.
%Pos=9
%Neg=775
%LL:-18.274555712422355
%LL:-58.54231160460086

% Running command: java -jar ../../../../Boostr/auc.jar ../data/test3/AUC/aucTemp.txt list 0.0
% WAITING FOR command: java -jar ../../../../Boostr/auc.jar ../data/test3/AUC/aucTemp.txt list 0.0
% DONE WAITING FOR command: java -jar ../../../../Boostr/auc.jar ../data/test3/AUC/aucTemp.txt list 0.0

%   AUC ROC   = 0.967240
%   AUC PR    = 0.279754
%   CLL	      = -0.074671
%   Precision = 0.056250 at threshold = 0.068
%   Recall    = 1.000000
%   F1        = 0.106509

% Total inference time (20 trees): 5.326 seconds.
