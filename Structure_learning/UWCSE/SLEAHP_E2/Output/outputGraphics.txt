Time=161.346000 
LL: -15159.467811855488
AUCROC: 0.9598486895433666
AUCPR: 0.07725888463834461
Learned Program
advisedby(A,B):0.04457053179536169:-professor(B).
advisedby(A,B):0.018310937670949767:-hasposition(B,C).
advisedby(A,B):0.8262192010863676:-taughtby(D,B,E).
advisedby(A,B):0.5028135743994673:-publication(F,B).
advisedby(A,B):0.9329934479635642:-publication(G,B).
advisedby(A,B):0.07538904633524063:-publication(H,B),hidden_7(A,H,B).
advisedby(A,B):0.374743923123271:-publication(I,B).
advisedby(A,B):3.446651579491755e-5:-publication(J,B),hidden_10(A,J,B).
advisedby(A,B):0.00014223281022973655:-taughtby(K,B,L),hidden_12(A,K,B,L).
advisedby(A,B):0.0005322680765916799:-taughtby(K,B,M),hidden_13(A,K,B,M).
advisedby(A,B):0.0005703522100192204:-taughtby(K,B,N),hidden_14(A,K,B,N).
hidden_7(A,H,B):2.069099949497666e-5:-publication(H,O).
hidden_10(A,J,B):0.06789270603144537:-publication(J,P).
hidden_12(A,K,B,L):0.303804013946702:-taughtby(Q,R,L).
hidden_13(A,K,B,M):0.15124375024736297:-taughtby(S,T,M).
hidden_13(A,K,B,M):0.9128301364789848:-ta(U,V,M).
hidden_14(A,K,B,N):0.2584071780722917:-taughtby(K,W,X).
hidden_14(A,K,B,N):0.29551706259451677:-ta(K,Y,Z).

 Hyperparameters of uwcse_deep:

Save statistics? no
Number of arithmetic circuits: 3831 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Regularization enabled
Regularization type:L2 
Gamma: 10,000000
