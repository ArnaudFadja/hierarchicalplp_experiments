% Running on host: r240n01

% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.

% Calling ILPouterLoop from createRegressionOuterLooper.

% getInputArgWithDefaultValue: args=[../data/test1/test1_pos.txt, ../data/test1/test1_neg.txt, ../data/test1/test1_bk.txt, ../data/test1/test1_facts.txt]
%  for N=0: args[N]=../data/test1/test1_pos.txt

% getInputArgWithDefaultValue: args=[../data/test1/test1_pos.txt, ../data/test1/test1_neg.txt, ../data/test1/test1_bk.txt, ../data/test1/test1_facts.txt]
%  for N=1: args[N]=../data/test1/test1_neg.txt

% getInputArgWithDefaultValue: args=[../data/test1/test1_pos.txt, ../data/test1/test1_neg.txt, ../data/test1/test1_bk.txt, ../data/test1/test1_facts.txt]
%  for N=2: args[N]=../data/test1/test1_bk.txt

% getInputArgWithDefaultValue: args=[../data/test1/test1_pos.txt, ../data/test1/test1_neg.txt, ../data/test1/test1_bk.txt, ../data/test1/test1_facts.txt]
%  for N=3: args[N]=../data/test1/test1_facts.txt

% Welcome to the WILL ILP/SRL systems.


% Switching to VarIndicator = uppercase.

% Unset'ing VarIndicator.
% Reading background theory from dir: null
% Load '../uwcse_bk.txt'.

% Switching to VarIndicator = uppercase.

% Switching to standard-logic notation for variables; previous setting = uppercase

% Switching to VarIndicator = lowercase.

***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****


***** Warning: % Since this is the first setting of the notation for variables, will keep:
%   variableIndicator = lowercase *****

% [ LazyGroundClauseIndex ]  Building full index for mode/1 with 1 assertions.
% LoadAllModes() called.  Currently loaded modes: []

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.

% Switching to Prolog notation for variables; previous setting = lowercase

% Switching to VarIndicator = uppercase.

% Switching to VarIndicator = lowercase.
% [ LazyGroundClauseIndex ]  Building full index for sameAs/2 with 2 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for exp/3.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for log/3.
% LoadAllLibraries() called.  Currently loaded libraries: [listsInLogic, differentInLogic, modes_arithmeticInLogic, inlines_comparisonInLogic, modes_listsInLogic, inlines_differentInLogic, modes_differentInLogic, arithmeticInLogic, inlines_listsInLogic, modes_comparisonInLogic, comparisonInLogic, inlines_arithmeticInLogic]

%  Read the facts.
%  Have read 731 facts.
% Have read 35 examples from '../data/test1' [../data/test1/test1*].
% Have read 4,589 examples from '../data/test1' [../data/test1/test1*].

%  LearnOneClause initialized.

% The outer looper has been created.

% Initializing the ILP inner looper.

% NEW target:                 advisedby(a, b)
%  targetPred:                advisedby/2
%  targetArgTypes:            signature = [Const, Const], types = [+Person, +Person]
%  targets:                   [advisedby(a, b)]
%  targetPredicates:          [advisedby/2]
%  targetArgSpecs:            [[a[+Person], b[+Person]]]
%  variablesInTargets:        [[a, b]]

% Started collecting constants

% Collecting the types of constants.

% Looking at the training examples to see if any types of new constants can be inferred.
% Time to collect constants: 462 milliseconds
% Time to collect examples: 0 seconds

% Read 35 pos examples and 4,589 neg examples.
% Time to init learnOneClause: 506 milliseconds
% Old dir../data/train1/models/

% Have 35 'raw' positive examples and kept 35.
% Have 4,589 'raw' negative examples and kept 4,589.

% processing backup's for advisedby
%  POS EX = 35
%  NEG EX = 4,589

% Memory usage by WILLSetup (just counts # targets?):
%  |backupPosExamples| = 1
%  |backupNegExamples| = 1
%  |predicatesAsFacts| = 0
%  |addedToFactBase|   = 0

% Getting bRDN's target predicates.
% Did not learn a model for 'advisedby' this run.
%   loadModel (#0): ../data/train1/models/bRDNs/Trees/advisedbyTree0.tree
%   loadModel (#1): ../data/train1/models/bRDNs/Trees/advisedbyTree1.tree
%   loadModel (#2): ../data/train1/models/bRDNs/Trees/advisedbyTree2.tree
%   loadModel (#3): ../data/train1/models/bRDNs/Trees/advisedbyTree3.tree
%   loadModel (#4): ../data/train1/models/bRDNs/Trees/advisedbyTree4.tree
%   loadModel (#5): ../data/train1/models/bRDNs/Trees/advisedbyTree5.tree
%   loadModel (#6): ../data/train1/models/bRDNs/Trees/advisedbyTree6.tree
%   loadModel (#7): ../data/train1/models/bRDNs/Trees/advisedbyTree7.tree
%   loadModel (#8): ../data/train1/models/bRDNs/Trees/advisedbyTree8.tree
%   loadModel (#9): ../data/train1/models/bRDNs/Trees/advisedbyTree9.tree
%   loadModel (#10): ../data/train1/models/bRDNs/Trees/advisedbyTree10.tree
%   loadModel (#11): ../data/train1/models/bRDNs/Trees/advisedbyTree11.tree
%   loadModel (#12): ../data/train1/models/bRDNs/Trees/advisedbyTree12.tree
%   loadModel (#13): ../data/train1/models/bRDNs/Trees/advisedbyTree13.tree
%   loadModel (#14): ../data/train1/models/bRDNs/Trees/advisedbyTree14.tree
%   loadModel (#15): ../data/train1/models/bRDNs/Trees/advisedbyTree15.tree
%   loadModel (#16): ../data/train1/models/bRDNs/Trees/advisedbyTree16.tree
%   loadModel (#17): ../data/train1/models/bRDNs/Trees/advisedbyTree17.tree
%   loadModel (#18): ../data/train1/models/bRDNs/Trees/advisedbyTree18.tree
%   loadModel (#19): ../data/train1/models/bRDNs/Trees/advisedbyTree19.tree
%  Done loading 20 models.
File: ../data/test1/advice.txt doesnt exist.Hence no advice loaded

% for advisedby |lookupPos| = 35
% for advisedby |lookupNeg| = 4,589
% getJointExamples: |pos| = 35, |neg| = 4,589

% Starting inference in bRDN.
% Trees = 20

% Starting getMarginalProbabilities.
% [ LazyGroundClauseIndex ]  Building full index for professor/1 with 14 assertions.
% [ LazyGroundClauseIndex ]  Building full index for student/1 with 54 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for publication/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for taughtBy/3.
% [ LazyGroundClauseIndex ]  Building full index for ta/3 with 20 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for hasPosition/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for ta/3.
% [ LazyGroundClauseIndex ]  Building full index for inPhase/2 with 47 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 1:  Building full index for tempAdvisedBy/2.
% [ LazyGroundClauseIndex ]  Building full index for courseLevel/2 with 30 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for ta/3.
% [ LazyGroundClauseIndex ]  Building full index for hasPosition/2 with 14 assertions.
% [ LazyGroundClauseIndex ]  Building full index for taughtBy/3 with 58 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for taughtBy/3.
% [ LazyGroundClauseIndex ]  Building full index for publication/2 with 292 assertions.
% [ LazyGroundClauseIndex ]  Building full index for yearsInProgram/2 with 47 assertions.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for tempAdvisedBy/2.
% [ LazyGroundNthArgumentClauseIndex ]  Argument 0:  Building full index for publication/2.
% No Gibbs sampling needed during inference.
 (Arithmetic) Mean Probability Assigned to Correct Output Class: 4,349.557/4,624.00 = 0.940648

 The weighted count of positive examples = 35.000 and the weighted count of negative examples = 4,589.000

printExamples: Writing out predictions (for Tuffy?) on 4,624 examples for 'advisedby' to:
  ../data/test1/results_advisedby.db
 and to:
  ../data/test1/query_advisedby.db
%    No need to compress since small: ../data/test1/query_advisedby.db

% Computing Area Under Curves.
%Pos=35
%Neg=4589
%LL:-47.982879534988626
%LL:-325.9589859987849

% Running command: java -jar ../../../../Boostr/auc.jar ../data/test1/AUC/aucTemp.txt list 0.0
% WAITING FOR command: java -jar ../../../../Boostr/auc.jar ../data/test1/AUC/aucTemp.txt list 0.0
% DONE WAITING FOR command: java -jar ../../../../Boostr/auc.jar ../data/test1/AUC/aucTemp.txt list 0.0

%   AUC ROC   = 0.958285
%   AUC PR    = 0.230777
%   CLL	      = -0.070493
%   Precision = 0.053435 at threshold = 0.076
%   Recall    = 1.000000
%   F1        = 0.101449

% Total inference time (20 trees): 9.387 seconds.
