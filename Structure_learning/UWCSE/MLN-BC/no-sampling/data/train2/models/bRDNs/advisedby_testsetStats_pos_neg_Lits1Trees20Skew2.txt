useLeadingQuestionMarkVariables: true.

// (Arithmetic) Mean Probability Assigned to Correct Output Class: 3,285.035/3,721.00 = 0.882837
testsetLikelihood(sum(3,285.035), countOfExamples(3,721.00), mean(0.882837)).

// The weighted count of positive examples = 20.000 and the weighted count of negative examples = 3,701.000
weightedSumPos(20.000).
weightedSumNeg(3,701.000).

//  AUC ROC   = 0.938483
//  AUC PR    = 0.042695
//  CLL       = -0.130898
aucROC(advisedby, 0.938483).
aucPR( advisedby, 0.042695).

//   Precision = 0.000000 at threshold = 0.122
//   Recall    = 0.000000
//   F1        = NaN
precision(advisedby, 0.000000, usingThreshold(0.12173294215979474)).
recall(   advisedby, 0.000000, usingThreshold(0.12173294215979474)).
F1(       advisedby, NaN, usingThreshold(0.12173294215979474)).

