Time=568.629000 
LL: -12121.192548720392
AUCROC: 0.9021881551362682
AUCPR: 0.03937463602449413
Learned Program
advisedby(A,B):0.031385124572238965:-professor(B).
advisedby(A,B):0.02289284626300514:-hasposition(B,C).
advisedby(A,B):0.2847118890778802:-publication(D,B).
advisedby(A,B):0.0026618583868184944:-taughtby(E,B,F),hidden_4(A,E,B,F).
advisedby(A,B):0.6381689574543998:-taughtby(G,B,H).
advisedby(A,B):0.12472025705044416:-taughtby(I,B,J),hidden_7(A,I,B,J).
advisedby(A,B):0.21837915506475838:-publication(K,B).
advisedby(A,B):0.024737627222930315:-taughtby(L,B,M),hidden_14(A,L,B,M).
advisedby(A,B):0.01460907148766637:-ta(N,A,O).
advisedby(A,B):0.2563609933637053:-publication(P,A).
advisedby(A,B):0.2701930398005061:-publication(Q,B).
advisedby(A,B):0.3487360121092934:-publication(R,B).
advisedby(A,B):0.001425151886724052:-taughtby(S,B,T),hidden_23(A,S,B,T).
advisedby(A,B):0.5423312989987608:-taughtby(S,B,U).
advisedby(A,B):0.8615634518406295:-publication(V,B).
hidden_4(A,E,B,F):0.08144950204041379:-ta(E,W,X).
hidden_7(A,I,B,J):0.09552366517361861:-ta(I,Y,J).
hidden_14(A,L,B,M):0.0011972255982982461:-taughtby(Z,A1,M).
hidden_23(A,S,B,T):0.023860334460550803:-taughtby(B1,C1,T).

 Hyperparameters of uwcse_deep:

Save statistics? no
Number of arithmetic circuits: 3831 
Max Iteration= 1000 
Epsilon= -0,000100 
Delta= -0,000010 
Value used as zero= 0,000000100
Seed =3035 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
