Wall time=0.314000  CLL=-14.906063 
LL: -105.49222590891709
AUCROC: 0.9627227463312369
AUCPR: 0.1173058085304205
Learned Program
advisedby(A,B):0.10005281062159281:-professor(B),hidden_1(A,B).
hidden_1(C,D):0.10021920596502949:-student(C),hidden_1_1(C,D).
hidden_1(E,F):0.10022412044118732:-student(E),hidden_1_2(E,F).
hidden_1_1(G,H):0.1003007280511005:-hasposition(H,faculty).
hidden_1_1(I,J):0.10030392867301026:-inphase(I,post_quals).
hidden_1_1(K,L):0.10037843378356287:-taughtby(M,L,N),hidden_1_1_1(K,L,M,N).
hidden_1_1(O,P):0.10037804353266433:-publication(Q,O),hidden_1_1_2(O,P,Q).
hidden_1_2(R,S):0.10043502825308565:-publication(T,R),hidden_1_2_1(R,S,T).
hidden_1_1_1(U,V,W,X):0.10011498265118743:-ta(W,U,X).
hidden_1_1_2(Y,Z,A1):0.10011973282946154:-publication(A1,Z).
hidden_1_2_1(B1,C1,D1):0.10037694472699317:-publication(D1,E1),hidden_1_2_1_1(B1,C1,D1,E1).
hidden_1_2_1_1(F1,G1,H1,I1):0.09961030975088608:-I1\==F1,hidden_1_2_1_1_1(F1,G1,H1,I1).
hidden_1_2_1_1_1(J1,K1,L1,M1):0.09772588023295382:-M1\==K1,hidden_1_2_1_1_1_1(J1,K1,L1,M1).
hidden_1_2_1_1_1_1(N1,O1,P1,Q1):0.09451966687262486:-inphase(N1,R1),hidden_1_2_1_1_1_1_1(N1,O1,P1,Q1,R1).
hidden_1_2_1_1_1_1(S1,T1,U1,V1):0.09451300483377537:-yearsinprogram(S1,W1),hidden_1_2_1_1_1_1_2(S1,T1,U1,V1,W1).
hidden_1_2_1_1_1_1_1(X1,Y1,Z1,A2,B2):0.08988715798285847:-inphase(A2,B2).
hidden_1_2_1_1_1_1_2(C2,D2,E2,F2,G2):0.08982117489096163:-yearsinprogram(F2,G2).

 Hyperparameters of uwcse_deep:

Save statistics? no
Number of arithmetic circuits: 3831 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Parameters are initialized with values in the range [-0,50 0,50]
Eta= 0,900000 
Beta1= 0,900000 
Beta2= 0,999000 
Adam_hat= 0,0000000100  
BatchSize= 100 
Strategy= stochastic 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
