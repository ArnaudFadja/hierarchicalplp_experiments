Wall time=4.846000  CLL=-94.119923 
LL: -10.174480211440192
AUCROC: 0.869047619047619
AUCPR: 0.9349575499391676
Learned Program
active:0.033617272106039306:-bond(A,B,1),hidden_1(A,B).
active:0.6445957367778646:-benzene(C),hidden_2(C).
active:0.06534238723049128:-nitro(D).
hidden_1(A,B):0.019939486845669308:-bond(E,A,7),hidden_1_2(B,E,A).
hidden_1(F,G):0.012667895413142106:-bond(G,H,7),hidden_1_3(F,G,H).
hidden_1(I,J):0.007428123067018078:-bond(K,I,7),hidden_1_4(J,K,I).
hidden_2(C):0.644599605966647:-ring_size_6(C).
hidden_1_2(B,E,A):0.12895086123653526:-bond(E,L,7),hidden_1_2_1(B,A,E,L).
hidden_1_3(F,G,H):2.478784418826763e-5:-atm(H,c,22,M),hidden_1_3_3(F,G,H,M).
hidden_1_4(J,K,I):0.0005799197296823877:-atm(K,c,22,N).
hidden_1_2_1(B,A,E,L):0.018865490574444265:-bond(L,O,7),hidden_1_2_1_1(B,A,E,L,O).
hidden_1_3_3(F,G,H,M):0.00043449602528653484:-atm(P,c,22,M).
hidden_1_2_1_1(B,A,E,L,O):0.022950431426795603:-bond(O,Q,7),hidden_1_2_1_1_1(B,A,E,L,O,Q).
hidden_1_2_1_1_1(B,A,E,L,O,Q):0.0024528718756414492:-bond(Q,R,7).

 Hyperparameters of muta_deep:

Save statistics? no
Number of arithmetic circuits: 169 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
