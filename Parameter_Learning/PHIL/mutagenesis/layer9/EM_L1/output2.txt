Wall time=4.952000  CLL=-94.302076 
LL: -10.160530318475057
AUCROC: 0.8999999999999999
AUCPR: 0.8975003391670058
Learned Program
active:0.03636486683453768:-bond(A,B,1),hidden_1(A,B).
active:0.6446217519125241:-benzene(C),hidden_2(C).
active:0.07126770099561242:-nitro(D).
hidden_1(A,B):0.020595946969706347:-bond(E,A,7),hidden_1_2(B,E,A).
hidden_1(F,G):0.015674877503442985:-bond(G,H,7),hidden_1_3(F,G,H).
hidden_1(I,J):0.008297945999152035:-bond(K,I,7),hidden_1_4(J,K,I).
hidden_2(C):0.6446268955314025:-ring_size_6(C).
hidden_1_2(B,E,A):0.13243630846063753:-bond(E,L,7),hidden_1_2_1(B,A,E,L).
hidden_1_3(F,G,H):3.763244931178633e-5:-atm(H,c,22,M),hidden_1_3_3(F,G,H,M).
hidden_1_4(J,K,I):0.0006773755267090564:-atm(K,c,22,N).
hidden_1_2_1(B,A,E,L):0.01971701683785341:-bond(L,O,7),hidden_1_2_1_1(B,A,E,L,O).
hidden_1_3_3(F,G,H,M):0.0006026123970059416:-atm(P,c,22,M).
hidden_1_2_1_1(B,A,E,L,O):0.024543780083547517:-bond(O,Q,7),hidden_1_2_1_1_1(B,A,E,L,O,Q).
hidden_1_2_1_1_1(B,A,E,L,O,Q):0.002624946537332562:-bond(Q,R,7).

 Hyperparameters of muta_deep:

Save statistics? no
Number of arithmetic circuits: 169 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
