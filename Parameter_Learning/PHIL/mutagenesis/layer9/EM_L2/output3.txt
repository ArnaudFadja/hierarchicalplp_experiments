Wall time=5.076000  CLL=-84.265797 
LL: -10.23138524025052
AUCROC: 0.8928571428571429
AUCPR: 0.9446187221003398
Learned Program
active:0.05205739469783699:-bond(A,B,1),hidden_1(A,B).
active:0.6329385229695192:-benzene(C),hidden_2(C).
active:0.005907866021855612:-benzene(D),hidden_3(D).
active:0.10916643071470525:-benzene(E),hidden_4(E).
active:0.0029282054596770912:-carbon_6_ring(F),hidden_5(F).
active:0.09400488320799316:-nitro(G).
hidden_1(A,B):0.013158001552376009:-bond(B,H,7),hidden_1_1(A,B,H).
hidden_1(I,J):0.10737145774311155:-bond(K,I,7),hidden_1_2(J,K,I).
hidden_1(L,M):0.19742341223466844:-bond(M,N,7),hidden_1_3(L,M,N).
hidden_1(O,P):0.14935121039231397:-bond(Q,O,7),hidden_1_4(P,Q,O).
hidden_1(R,S):0.0006119801904581612:-atm(S,c,194,T),hidden_1_5(R,S,T).
hidden_2(C):0.6329388274777246:-ring_size_6(C).
hidden_3(D):0.017767392191525344:-ring_size_6(D).
hidden_4(E):0.10925088251537146:-ring_size_6(E).
hidden_5(F):0.002928121928864824:-ring_size_6(F).
hidden_1_1(A,B,H):0.010787113037846185:-bond(H,U,1),hidden_1_1_1(A,B,H,U).
hidden_1_2(J,K,I):0.32862525014091504:-bond(K,V,7),hidden_1_2_1(J,I,K,V).
hidden_1_3(L,M,N):0.0481607956067458:-bond(N,W,7),hidden_1_3_1(L,M,N,W).
hidden_1_3(X,Y,Z):0.0520753772488583:-bond(Z,A1,1),hidden_1_3_2(X,Y,Z,A1).
hidden_1_3(B1,C1,D1):0.0658336854377124:-atm(D1,c,22,E1),hidden_1_3_3(B1,C1,D1,E1).
hidden_1_4(P,Q,O):0.052558527186987725:-bond(Q,F1,1),hidden_1_4_1(P,O,Q,F1).
hidden_1_5(R,S,T):0.0006114163708857423:-atm(G1,c,29,T).
hidden_1_1_1(A,B,H,U):0.0007063321147300905:-bond(U,H1,7),hidden_1_1_1_1(A,B,H,U,H1).
hidden_1_1_1(I1,J1,K1,L1):0.012167599428898723:-bond(L1,M1,7),hidden_1_1_1_2(I1,J1,K1,L1,M1).
hidden_1_2_1(J,I,K,V):0.17981298415788077:-bond(V,N1,7),hidden_1_2_1_1(J,I,K,V,N1).
hidden_1_3_1(L,M,N,W):0.04003560700713976:-bond(W,O1,7),hidden_1_3_1_1(L,M,N,W,O1).
hidden_1_3_2(X,Y,Z,A1):0.060797226884573397:-atm(A1,h,3,P1),hidden_1_3_2_1(X,Y,Z,A1,P1).
hidden_1_3_3(B1,C1,D1,E1):0.13982051656333283:-atm(Q1,c,22,E1).
hidden_1_4_1(P,O,Q,F1):0.09268799663254021:-atm(F1,h,3,R1).
hidden_1_1_1_1(A,B,H,U,H1):0.0007050248635785961:-bond(H1,S1,1).
hidden_1_1_1_2(I1,J1,K1,L1,M1):0.0023541814438992614:-bond(M1,T1,7),hidden_1_1_1_2_1(I1,J1,K1,L1,M1,T1).
hidden_1_1_1_2(U1,V1,W1,X1,Y1):0.003952373073838833:-bond(Y1,Z1,1),hidden_1_1_1_2_2(U1,V1,W1,X1,Y1,Z1).
hidden_1_1_1_2(A2,B2,C2,D2,E2):0.005496169243373927:-atm(E2,c,22,F2),hidden_1_1_1_2_3(A2,B2,C2,D2,E2,F2).
hidden_1_2_1_1(J,I,K,V,N1):0.2562001206797458:-bond(N1,G2,7),hidden_1_2_1_1_1(J,I,K,V,N1,G2).
hidden_1_3_1_1(L,M,N,W,O1):0.02387517317824417:-bond(O1,H2,7),hidden_1_3_1_1_1(L,M,N,W,O1,H2).
hidden_1_3_2_1(X,Y,Z,A1,P1):0.09052561163015146:-atm(I2,h,3,P1).
hidden_1_1_1_2_1(I1,J1,K1,L1,M1,T1):0.0017288576831752667:-bond(T1,J2,7),hidden_1_1_1_2_1_1(I1,J1,K1,L1,M1,T1,J2).
hidden_1_1_1_2_2(U1,V1,W1,X1,Y1,Z1):0.004020389674748548:-atm(Z1,h,3,K2),hidden_1_1_1_2_2_1(U1,V1,W1,X1,Y1,Z1,K2).
hidden_1_1_1_2_3(A2,B2,C2,D2,E2,F2):0.0054613884871179685:-atm(L2,c,22,F2).
hidden_1_2_1_1_1(J,I,K,V,N1,G2):0.13782797580994:-bond(G2,M2,7).
hidden_1_3_1_1_1(L,M,N,W,O1,H2):0.02487386688121168:-bond(H2,N2,7).
hidden_1_1_1_2_1_1(I1,J1,K1,L1,M1,T1,J2):0.0006067587606243263:-bond(J2,O2,7),hidden_1_1_1_2_1_1_1(I1,J1,K1,L1,M1,T1,J2,O2).
hidden_1_1_1_2_1_1(P2,Q2,R2,S2,T2,U2,V2):0.001112945115784747:-bond(V2,W2,1),hidden_1_1_1_2_1_1_2(P2,Q2,R2,S2,T2,U2,V2,W2).
hidden_1_1_1_2_2_1(U1,V1,W1,X1,Y1,Z1,K2):0.003845614555397392:-atm(X2,h,3,K2).
hidden_1_1_1_2_1_1_1(I1,J1,K1,L1,M1,T1,J2,O2):0.000607067311977072:-bond(O2,Y2,7).
hidden_1_1_1_2_1_1_2(P2,Q2,R2,S2,T2,U2,V2,W2):0.0004039187188153992:-bond(W2,Z2,2),hidden_1_1_1_2_1_1_2_1(P2,Q2,R2,S2,T2,U2,V2,W2,Z2).
hidden_1_1_1_2_1_1_2(A3,B3,C3,D3,E3,F3,G3,H3):0.00039294352567087465:-bond(H3,I3,2),hidden_1_1_1_2_1_1_2_2(A3,B3,C3,D3,E3,F3,G3,H3,I3).
hidden_1_1_1_2_1_1_2_1(P2,Q2,R2,S2,T2,U2,V2,W2,Z2):0.00040206642301698903:-atm(Z2,o,40,J3).
hidden_1_1_1_2_1_1_2_2(A3,B3,C3,D3,E3,F3,G3,H3,I3):0.0004038698154383291:-atm(I3,o,40,K3).

 Hyperparameters of muta_deep:

Save statistics? no
Number of arithmetic circuits: 169 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Regularization enabled
Regularization type:L2 
Gamma: 10,000000
