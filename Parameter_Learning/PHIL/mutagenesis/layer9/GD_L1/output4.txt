Wall time=5.088000  CLL=-93.405691 
LL: -20.527721318585495
AUCROC: 0.8125
AUCPR: 0.9635567047331755
Learned Program
active:0.09994573716144214:-lumo(A).
active:0.09996524993629874:-bond(B,C,1),hidden_1(B,C).
active:0.09999042752356982:-benzene(D),hidden_2(D).
active:0.09998529521196385:-benzene(E),hidden_3(E).
active:0.099987228921551:-benzene(F),hidden_4(F).
active:0.10003734463619514:-carbon_6_ring(G),hidden_5(G).
active:0.09995646530812555:-nitro(H).
hidden_1(I,J):0.09979726279932162:-bond(J,K,7),hidden_1_1(I,J,K).
hidden_1(L,M):0.0996813648291784:-bond(N,L,7),hidden_1_2(M,N,L).
hidden_1(O,P):0.09970055590850976:-bond(P,Q,7),hidden_1_3(O,P,Q).
hidden_1(R,S):0.09966533351620681:-bond(T,R,7),hidden_1_4(S,T,R).
hidden_1(U,V):0.0996439168391769:-atm(U,c,194,W).
hidden_1(X,Y):0.09978140157326088:-atm(Y,c,194,Z),hidden_1_5(X,Y,Z).
hidden_2(A1):0.0997862358782608:-ring_size_6(A1).
hidden_3(B1):0.0995129163389463:-ring_size_6(B1).
hidden_4(C1):0.09965120960061748:-ring_size_6(C1).
hidden_5(D1):0.09995695239881248:-ring_size_6(D1).
hidden_1_1(E1,F1,G1):0.09903215302303342:-bond(G1,H1,1),hidden_1_1_1(E1,F1,G1,H1).
hidden_1_2(I1,J1,K1):0.0982434681557618:-bond(J1,L1,7),hidden_1_2_1(I1,K1,J1,L1).
hidden_1_3(M1,N1,O1):0.09853050912333082:-bond(O1,P1,7),hidden_1_3_1(M1,N1,O1,P1).
hidden_1_3(Q1,R1,S1):0.09851409834928325:-bond(S1,T1,1),hidden_1_3_2(Q1,R1,S1,T1).
hidden_1_3(U1,V1,W1):0.0984579212723275:-atm(W1,c,22,X1),hidden_1_3_3(U1,V1,W1,X1).
hidden_1_4(Y1,Z1,A2):0.09857545708618307:-bond(Z1,B2,1),hidden_1_4_1(Y1,A2,Z1,B2).
hidden_1_4(C2,D2,E2):0.09840585508535246:-atm(D2,c,22,F2).
hidden_1_5(G2,H2,I2):0.09890037525613947:-atm(J2,c,29,I2).
hidden_1_1_1(K2,L2,M2,N2):0.09712690678458677:-bond(N2,O2,7),hidden_1_1_1_1(K2,L2,M2,N2,O2).
hidden_1_1_1(P2,Q2,R2,S2):0.0972235776126557:-bond(S2,T2,7),hidden_1_1_1_2(P2,Q2,R2,S2,T2).
hidden_1_2_1(U2,V2,W2,X2):0.09498172899528186:-bond(X2,Y2,7),hidden_1_2_1_1(U2,V2,W2,X2,Y2).
hidden_1_2_1(Z2,A3,B3,C3):0.09445539133336006:-bond(C3,D3,1).
hidden_1_3_1(E3,F3,G3,H3):0.09569017277011617:-bond(H3,I3,7),hidden_1_3_1_1(E3,F3,G3,H3,I3).
hidden_1_3_1(J3,K3,L3,M3):0.09503331537353762:-bond(M3,N3,1).
hidden_1_3_2(O3,P3,Q3,R3):0.09541260157969671:-atm(R3,h,3,S3),hidden_1_3_2_1(O3,P3,Q3,R3,S3).
hidden_1_3_3(T3,U3,V3,W3):0.09495236504532147:-atm(X3,c,22,W3).
hidden_1_4_1(Y3,Z3,A4,B4):0.09580574016391506:-atm(B4,h,3,C4).
hidden_1_1_1_1(D4,E4,F4,G4,H4):0.09319466082670178:-bond(H4,I4,1).
hidden_1_1_1_2(J4,K4,L4,M4,N4):0.09428230917023425:-bond(N4,O4,7),hidden_1_1_1_2_1(J4,K4,L4,M4,N4,O4).
hidden_1_1_1_2(P4,Q4,R4,S4,T4):0.09423570539281:-bond(T4,U4,1),hidden_1_1_1_2_2(P4,Q4,R4,S4,T4,U4).
hidden_1_1_1_2(V4,W4,X4,Y4,Z4):0.09417053201669665:-atm(Z4,c,22,A5),hidden_1_1_1_2_3(V4,W4,X4,Y4,Z4,A5).
hidden_1_2_1_1(B5,C5,D5,E5,F5):0.09020422598141804:-bond(F5,G5,7),hidden_1_2_1_1_1(B5,C5,D5,E5,F5,G5).
hidden_1_2_1_1(H5,I5,J5,K5,L5):0.08945247832482535:-bond(L5,M5,1).
hidden_1_3_1_1(N5,O5,P5,Q5,R5):0.09131896346427:-bond(R5,S5,7),hidden_1_3_1_1_1(N5,O5,P5,Q5,R5,S5).
hidden_1_3_1_1(T5,U5,V5,W5,X5):0.09065536245548139:-bond(X5,Y5,1).
hidden_1_3_2_1(Z5,A6,B6,C6,D6):0.08957421745161112:-atm(E6,h,3,D6).
hidden_1_1_1_2_1(F6,G6,H6,I6,J6,K6):0.0911919273762833:-bond(K6,L6,7),hidden_1_1_1_2_1_1(F6,G6,H6,I6,J6,K6,L6).
hidden_1_1_1_2_1(M6,N6,O6,P6,Q6,R6):0.09077452764143235:-bond(R6,S6,1).
hidden_1_1_1_2_2(T6,U6,V6,W6,X6,Y6):0.09043045512873474:-atm(Y6,h,3,Z6),hidden_1_1_1_2_2_1(T6,U6,V6,W6,X6,Y6,Z6).
hidden_1_1_1_2_3(A7,B7,C7,D7,E7,F7):0.08995301867697458:-atm(G7,c,22,F7).
hidden_1_2_1_1_1(H7,I7,J7,K7,L7,M7):0.08549792420048889:-bond(M7,N7,7).
hidden_1_3_1_1_1(O7,P7,Q7,R7,S7,T7):0.08716906156192328:-bond(T7,U7,7).
hidden_1_1_1_2_1_1(V7,W7,X7,Y7,Z7,A8,B8):0.0889211050248027:-bond(B8,C8,7),hidden_1_1_1_2_1_1_1(V7,W7,X7,Y7,Z7,A8,B8,C8).
hidden_1_1_1_2_1_1(D8,E8,F8,G8,H8,I8,J8):0.08891396502420984:-bond(J8,K8,1),hidden_1_1_1_2_1_1_2(D8,E8,F8,G8,H8,I8,J8,K8).
hidden_1_1_1_2_2_1(L8,M8,N8,O8,P8,Q8,R8):0.08672342106134251:-atm(S8,h,3,R8).
hidden_1_1_1_2_1_1_1(T8,U8,V8,W8,X8,Y8,Z8,A9):0.08751260741503328:-bond(A9,B9,7).
hidden_1_1_1_2_1_1_2(C9,D9,E9,F9,G9,H9,I9,J9):0.08733994187548651:-bond(J9,K9,2),hidden_1_1_1_2_1_1_2_1(C9,D9,E9,F9,G9,H9,I9,J9,K9).
hidden_1_1_1_2_1_1_2(L9,M9,N9,O9,P9,Q9,R9,S9):0.08728921296713696:-bond(S9,T9,2),hidden_1_1_1_2_1_1_2_2(L9,M9,N9,O9,P9,Q9,R9,S9,T9).
hidden_1_1_1_2_1_1_2(U9,V9,W9,X9,Y9,Z9,A10,B10):0.08717882425014996:-atm(B10,n,38,C10).
hidden_1_1_1_2_1_1_2_1(D10,E10,F10,G10,H10,I10,J10,K10,L10):0.08571656997623907:-atm(L10,o,40,M10).
hidden_1_1_1_2_1_1_2_2(N10,O10,P10,Q10,R10,S10,T10,U10,V10):0.08491068163591978:-atm(V10,o,40,W10).

 Hyperparameters of muta_deep:

Save statistics? no
Number of arithmetic circuits: 169 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Parameters are initialized with values in the range [-0,50 0,50]
Eta= 0,900000 
Beta1= 0,900000 
Beta2= 0,999000 
Adam_hat= 0,0000000100  
BatchSize= 0 
Strategy= batch 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
