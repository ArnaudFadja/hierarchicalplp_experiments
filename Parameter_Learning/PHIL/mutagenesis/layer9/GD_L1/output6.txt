Wall time=5.159000  CLL=-96.012379 
LL: -17.909062075888038
AUCROC: 0.8571428571428572
AUCPR: 0.9301179970822829
Learned Program
active:0.09995309032696965:-lumo(A).
active:0.0999689684008026:-bond(B,C,1),hidden_1(B,C).
active:0.0999943234942143:-benzene(D),hidden_2(D).
active:0.09998921455870602:-benzene(E),hidden_3(E).
active:0.09999113957336304:-benzene(F),hidden_4(F).
active:0.10003733921491784:-carbon_6_ring(G),hidden_5(G).
active:0.09996147233902093:-nitro(H).
hidden_1(I,J):0.0998011544806186:-bond(J,K,7),hidden_1_1(I,J,K).
hidden_1(L,M):0.09969316143977011:-bond(N,L,7),hidden_1_2(M,N,L).
hidden_1(O,P):0.09970179920568367:-bond(P,Q,7),hidden_1_3(O,P,Q).
hidden_1(R,S):0.0996789356290283:-bond(T,R,7),hidden_1_4(S,T,R).
hidden_1(U,V):0.09964786935508056:-atm(U,c,194,W).
hidden_1(X,Y):0.09978528932078602:-atm(Y,c,194,Z),hidden_1_5(X,Y,Z).
hidden_2(A1):0.09979604125665316:-ring_size_6(A1).
hidden_3(B1):0.09952379007323267:-ring_size_6(B1).
hidden_4(C1):0.09966154998212463:-ring_size_6(C1).
hidden_5(D1):0.09995694810458587:-ring_size_6(D1).
hidden_1_1(E1,F1,G1):0.09903639576184112:-bond(G1,H1,1),hidden_1_1_1(E1,F1,G1,H1).
hidden_1_2(I1,J1,K1):0.09827101926876947:-bond(J1,L1,7),hidden_1_2_1(I1,K1,J1,L1).
hidden_1_3(M1,N1,O1):0.09852787952210981:-bond(O1,P1,7),hidden_1_3_1(M1,N1,O1,P1).
hidden_1_3(Q1,R1,S1):0.09851281199960465:-bond(S1,T1,1),hidden_1_3_2(Q1,R1,S1,T1).
hidden_1_3(U1,V1,W1):0.09845146356458671:-atm(W1,c,22,X1),hidden_1_3_3(U1,V1,W1,X1).
hidden_1_4(Y1,Z1,A2):0.09860695418352669:-bond(Z1,B2,1),hidden_1_4_1(Y1,A2,Z1,B2).
hidden_1_4(C2,D2,E2):0.09843448179473921:-atm(D2,c,22,F2).
hidden_1_5(G2,H2,I2):0.09890445011951769:-atm(J2,c,29,I2).
hidden_1_1_1(K2,L2,M2,N2):0.09712950858346507:-bond(N2,O2,7),hidden_1_1_1_1(K2,L2,M2,N2,O2).
hidden_1_1_1(P2,Q2,R2,S2):0.09722623218626397:-bond(S2,T2,7),hidden_1_1_1_2(P2,Q2,R2,S2,T2).
hidden_1_2_1(U2,V2,W2,X2):0.09503090185038401:-bond(X2,Y2,7),hidden_1_2_1_1(U2,V2,W2,X2,Y2).
hidden_1_2_1(Z2,A3,B3,C3):0.09450051559942263:-bond(C3,D3,1).
hidden_1_3_1(E3,F3,G3,H3):0.09568024668987997:-bond(H3,I3,7),hidden_1_3_1_1(E3,F3,G3,H3,I3).
hidden_1_3_1(J3,K3,L3,M3):0.09501286399378951:-bond(M3,N3,1).
hidden_1_3_2(O3,P3,Q3,R3):0.09540594572310855:-atm(R3,h,3,S3),hidden_1_3_2_1(O3,P3,Q3,R3,S3).
hidden_1_3_3(T3,U3,V3,W3):0.09492964725066465:-atm(X3,c,22,W3).
hidden_1_4_1(Y3,Z3,A4,B4):0.09586362343665072:-atm(B4,h,3,C4).
hidden_1_1_1_1(D4,E4,F4,G4,H4):0.09319438735365984:-bond(H4,I4,1).
hidden_1_1_1_2(J4,K4,L4,M4,N4):0.09428174355957916:-bond(N4,O4,7),hidden_1_1_1_2_1(J4,K4,L4,M4,N4,O4).
hidden_1_1_1_2(P4,Q4,R4,S4,T4):0.09423580638526113:-bond(T4,U4,1),hidden_1_1_1_2_2(P4,Q4,R4,S4,T4,U4).
hidden_1_1_1_2(V4,W4,X4,Y4,Z4):0.09417060724068693:-atm(Z4,c,22,A5),hidden_1_1_1_2_3(V4,W4,X4,Y4,Z4,A5).
hidden_1_2_1_1(B5,C5,D5,E5,F5):0.09026700553335981:-bond(F5,G5,7),hidden_1_2_1_1_1(B5,C5,D5,E5,F5,G5).
hidden_1_2_1_1(H5,I5,J5,K5,L5):0.0895196540346606:-bond(L5,M5,1).
hidden_1_3_1_1(N5,O5,P5,Q5,R5):0.09130317273197412:-bond(R5,S5,7),hidden_1_3_1_1_1(N5,O5,P5,Q5,R5,S5).
hidden_1_3_1_1(T5,U5,V5,W5,X5):0.09064365213398494:-bond(X5,Y5,1).
hidden_1_3_2_1(Z5,A6,B6,C6,D6):0.08956370221624424:-atm(E6,h,3,D6).
hidden_1_1_1_2_1(F6,G6,H6,I6,J6,K6):0.09119029345458139:-bond(K6,L6,7),hidden_1_1_1_2_1_1(F6,G6,H6,I6,J6,K6,L6).
hidden_1_1_1_2_1(M6,N6,O6,P6,Q6,R6):0.09077279331207844:-bond(R6,S6,1).
hidden_1_1_1_2_2(T6,U6,V6,W6,X6,Y6):0.09042856429751951:-atm(Y6,h,3,Z6),hidden_1_1_1_2_2_1(T6,U6,V6,W6,X6,Y6,Z6).
hidden_1_1_1_2_3(A7,B7,C7,D7,E7,F7):0.08995103903023048:-atm(G7,c,22,F7).
hidden_1_2_1_1_1(H7,I7,J7,K7,L7,M7):0.08555603827576509:-bond(M7,N7,7).
hidden_1_3_1_1_1(O7,P7,Q7,R7,S7,T7):0.08715249324232219:-bond(T7,U7,7).
hidden_1_1_1_2_1_1(V7,W7,X7,Y7,Z7,A8,B8):0.08891888983901919:-bond(B8,C8,7),hidden_1_1_1_2_1_1_1(V7,W7,X7,Y7,Z7,A8,B8,C8).
hidden_1_1_1_2_1_1(D8,E8,F8,G8,H8,I8,J8):0.08891207910347679:-bond(J8,K8,1),hidden_1_1_1_2_1_1_2(D8,E8,F8,G8,H8,I8,J8,K8).
hidden_1_1_1_2_2_1(L8,M8,N8,O8,P8,Q8,R8):0.0867208695207857:-atm(S8,h,3,R8).
hidden_1_1_1_2_1_1_1(T8,U8,V8,W8,X8,Y8,Z8,A9):0.08751058405175076:-bond(A9,B9,7).
hidden_1_1_1_2_1_1_2(C9,D9,E9,F9,G9,H9,I9,J9):0.08733780060361407:-bond(J9,K9,2),hidden_1_1_1_2_1_1_2_1(C9,D9,E9,F9,G9,H9,I9,J9,K9).
hidden_1_1_1_2_1_1_2(L9,M9,N9,O9,P9,Q9,R9,S9):0.0872870671527222:-bond(S9,T9,2),hidden_1_1_1_2_1_1_2_2(L9,M9,N9,O9,P9,Q9,R9,S9,T9).
hidden_1_1_1_2_1_1_2(U9,V9,W9,X9,Y9,Z9,A10,B10):0.08717672503564827:-atm(B10,n,38,C10).
hidden_1_1_1_2_1_1_2_1(D10,E10,F10,G10,H10,I10,J10,K10,L10):0.0857148701697403:-atm(L10,o,40,M10).
hidden_1_1_1_2_1_1_2_2(N10,O10,P10,Q10,R10,S10,T10,U10,V10):0.08490893071399015:-atm(V10,o,40,W10).

 Hyperparameters of muta_deep:

Save statistics? no
Number of arithmetic circuits: 169 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Parameters are initialized with values in the range [-0,50 0,50]
Eta= 0,900000 
Beta1= 0,900000 
Beta2= 0,999000 
Adam_hat= 0,0000000100  
BatchSize= 0 
Strategy= batch 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
