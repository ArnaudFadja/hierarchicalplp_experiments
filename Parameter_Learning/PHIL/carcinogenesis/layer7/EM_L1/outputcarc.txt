Wall time=181.089000  CLL=-613.389177 
LL: -48.66899182097376
AUCROC: 0.6842105263157895
AUCPR: 0.6797115121145118
Learned Program
active:0.4980426509294986:-ames.
active:0.2146296081700619:-has_property(salmonella,p).
active:0.05471484133260844:-ashby_alert(di10,A).
active:0.1077263034239877:-atm(B,c,22,C),hidden_4(B,C).
active:0.008052814151085386:-atm(D,c,22,E).
active:0.08965104029850153:-amine(F).
hidden_4(B,C):0.023550814283680666:-atm(G,c,22,C),hidden_4_1(B,G,C).
hidden_4(H,I):0.00918974563910524:-atm(J,c,22,I),hidden_4_2(H,J,I).
hidden_4(K,L):0.007873909615955198:-atm(M,c,22,L),hidden_4_3(K,M,L).
hidden_4(N,O):0.23507360912553849:-symbond(P,N,7),hidden_4_4(O,P,N).
hidden_4_1(B,G,C):0.5750649730889563:-symbond(Q,G,1).
hidden_4_2(H,J,I):0.38359309680890874:-symbond(R,J,1).
hidden_4_3(K,M,L):0.1564000729178588:-symbond(S,M,1).
hidden_4_4(O,P,N):0.15923741329161203:-symbond(P,T,1).
hidden_4_4(U,V,W):0.3080460092809517:-symbond(X,V,7),hidden_4_4_1(U,W,X,V).
hidden_4_4_1(U,W,X,V):0.05164014367310301:-symbond(X,Y,1).
hidden_4_4_1(Z,A1,B1,C1):0.22770807033521123:-symbond(D1,B1,7),hidden_4_4_1_1(Z,A1,C1,D1,B1).
hidden_4_4_1_1(Z,A1,C1,D1,B1):0.5112386317789059:-symbond(D1,E1,1).
hidden_4_4_1_1(F1,G1,H1,I1,J1):0.2261997752910247:-symbond(K1,I1,7),hidden_4_4_1_1_1(F1,G1,H1,J1,K1,I1).
hidden_4_4_1_1_1(F1,G1,H1,J1,K1,I1):0.9362119537232502:-symbond(K1,L1,1).
hidden_4_4_1_1_1(M1,N1,O1,P1,Q1,R1):0.3247494489085511:-symbond(S1,Q1,7),hidden_4_4_1_1_1_1(M1,N1,O1,P1,R1,S1,Q1).
hidden_4_4_1_1_1_1(M1,N1,O1,P1,R1,S1,Q1):0.3698107337757392:-symbond(S1,T1,1).

 Hyperparameters of carc_deep:

Save statistics? no
Number of arithmetic circuits: 298 
Max Iteration= 1000 
Epsilon= 0,000100 
Delta= 0,000010 
Value used as zero= 0,000000100
Seed =3035 
Regularization enabled
Regularization type:L1 
Gamma: 10,000000
